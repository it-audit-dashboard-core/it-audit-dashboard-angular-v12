import { NgModule } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { merge } from 'rxjs';
import { startWith, switchMap } from 'rxjs/operators';
import { DataInformationOutputDto } from 'src/app/layout/models/user-application';
import { ValidatedResultDto, ValidationFailureDto } from 'src/app/layout/models/validator-result';
import { DataInformationService } from 'src/app/layout/services/data-information.service';

@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.css']
})
export class NotificationsComponent implements OnInit {
  pageKey: string = "";
  dataInformationResultDto!: ValidatedResultDto<DataInformationOutputDto>;
  validationFailures!: ValidationFailureDto[];

  constructor(
    private router: Router,
    private dataInformationService: DataInformationService) { }

  ngOnInit(): void {
    this.pageKey = this.router.url.replace("/apps/", "");
    this.getDataInformation(this.pageKey);
  }

  getDataInformation(key:string) {
    merge()
    .pipe(
      startWith({}),
      switchMap(() => {
        console.log(key);
        return this.dataInformationService.get(key);
      }),
    ).subscribe(
      response => {
        console.log(response);
        this.dataInformationResultDto = response as ValidatedResultDto<DataInformationOutputDto>;
        this.validationFailures = this.dataInformationResultDto.errors;
      },
      error => { 
        console.log(error) 
      },
      () => console.log('getUserApplications() complete')
    )
  }
  
  setIcon(severity: number): string {
    switch (severity){ 
      case 1: 
        return "fa fa-exclamation-triangle pl-2 pr-2"; 
      default: 
        return "fa fa-exclamation-triangle pl-2 pr-2" 
      }
  }

  setClass(severity: number): string {
    switch (severity){ 
      case 1: 
        return "alert alert-danger show"; 
      default: 
        return "alert alert-warning show" 
    }
  }
}
