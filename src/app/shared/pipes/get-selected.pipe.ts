import { Injectable, Pipe, PipeTransform } from "@angular/core";

@Pipe({
    name: 'get-selected',
    pure: false
})
@Injectable()
export class GetSelectedPipe implements PipeTransform {
    transform(items: any[]): any {
        // take out only selected values
        return items.filter(item => item.selected === true);
    }
}