import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GetSelectedPipe } from './pipes/get-selected.pipe';
import { DateInputsComponent } from './components/date-inputs/date-inputs.component';

@NgModule({
  declarations: [
    GetSelectedPipe,
    DateInputsComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    //NotificationsComponent
  ]
})
export class SharedModule { }
