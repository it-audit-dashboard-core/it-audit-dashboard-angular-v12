import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { DataSourceOutputDto } from '../../models/user-application';
import { ValidationFailureDto } from '../../models/validator-result';

@Component({
  selector: 'app-data-sources',
  templateUrl: './data-sources.component.html',
  styleUrls: ['./data-sources.component.css']
})
export class DataSourcesComponent implements OnInit {
  @Input() dataSources!: DataSourceOutputDto[];
  @Input() validationFailures!: ValidationFailureDto[];
  dataSourceForm!: FormGroup;
  
  constructor() { }

  ngOnInit(): void {
  }
  
  getModifiedDate(index:number):string{
    return "modifiedDate" + index;
  }

  setIcon(severity: number): string {
    switch (severity){ 
      case 1: 
        return "fa fa-exclamation-triangle pl-2 pr-2"; 
      default: 
        return "fa fa-exclamation-triangle pl-2 pr-2" 
      }
  }

  setClass(severity: number): string {
    switch (severity){ 
      case 1: 
        return "alert alert-danger fade in alert-dismissible show p-1 mt-1"; 
      default: 
        return "alert alert-warning fade in alert-dismissible show p-1 mt-1" 
    }
  }

  showAlert(){
    return 'list-group-item list-group-item-danger';// : 'list-group-item list-group-item-warning'
  }
}
