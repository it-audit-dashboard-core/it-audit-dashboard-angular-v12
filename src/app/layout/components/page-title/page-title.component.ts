import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { merge } from 'rxjs';
import { startWith, switchMap } from 'rxjs/operators';
import { IdentityUser } from '../../models/identity-user';
import { DataInformationOutputDto, DataSourceDto, UserApplicationDto } from '../../models/user-application';
import { ValidatedResultDto, ValidationFailureDto } from '../../models/validator-result';
import { DataInformationService } from '../../services/data-information.service';
import { UserApplicationService } from '../../services/user-application.service';

@Component({
  selector: 'app-page-title',
  templateUrl: './page-title.component.html',
  styleUrls: ['./page-title.component.css']
})
export class PageTitleComponent implements OnInit {
  pageKey: string = "";
  pageTitle: string = "";
  userApplicationResultDto!: ValidatedResultDto<UserApplicationDto>;
  dataInformationResultDto!: ValidatedResultDto<DataInformationOutputDto>;
  identityUser!: IdentityUser;
  dataSources!: DataSourceDto[];
  validationFailures!: ValidationFailureDto[];

  constructor(
    private router: Router,
    private userApplicationService: UserApplicationService,
    private dataInformationService: DataInformationService) { }

  ngOnInit(): void {
    this.pageKey = this.router.url.replace("/apps/", "");
    console.log('DI: ' + this.pageKey);
    this.getUserApplication(this.pageKey);
    this.getDataInformation(this.pageKey);
  }

  getUserApplication(key:string) {
    merge()
      .pipe(
        startWith({}),
        switchMap(() => {
          return this.userApplicationService.getByKeyAsync(key);
        }),
      ).subscribe(
        response => {
          this.userApplicationResultDto = response as ValidatedResultDto<UserApplicationDto>;
          this.pageTitle = this.userApplicationResultDto.resultDto.title;
          console.log(this.pageTitle);
        },
        error => { 
          console.log(error) 
        },
        () => console.log('getUserApplications() complete')
      )
  }

  getDataInformation(key:string) {
    merge()
    .pipe(
      startWith({}),
      switchMap(() => {
        console.log(key);
        return this.dataInformationService.get(key);
      }),
    ).subscribe(
      response => {
        console.log(response);
        this.dataInformationResultDto = response as ValidatedResultDto<DataInformationOutputDto>;
        this.dataSources = this.dataInformationResultDto.resultDto.dataSources;
        this.validationFailures = this.dataInformationResultDto.errors;
      },
      error => { 
        console.log(error) 
      },
      () => console.log('getUserApplications() complete')
    )
  }
}