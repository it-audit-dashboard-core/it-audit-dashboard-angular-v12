import { Component, OnInit } from '@angular/core';
import { merge } from 'rxjs';
import { startWith, switchMap } from 'rxjs/operators';
import { IdentityUser } from '../../models/identity-user';
import { ValidatedResultDto, ValidationFailureDto } from '../../models/validator-result';
import { DataInformationOutputDto, DataSourceDto, UserApplicationDto } from '../../models/user-application';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { IdentityService } from '../../services/identity.service';

@Component({
  selector: 'app-page-header',
  templateUrl: './page-header.component.html',
  styleUrls: ['./page-header.component.css']
})
export class PageHeaderComponent implements OnInit {
  environmentSettingsForm!: FormGroup;
  userNameForm!: FormGroup;
  userNameControl!: FormControl;
  environmentMachine: FormControl | undefined;

  identityUser!: IdentityUser;
  userApplicationResultDto!: ValidatedResultDto<UserApplicationDto>;
  dataInformationResultDto!: ValidatedResultDto<DataInformationOutputDto>;
  dataSources!: DataSourceDto[];
  validationFailures!: ValidationFailureDto[];
  userName: string = "";

  constructor(private formBuilder: FormBuilder, private accountService: IdentityService) { }

  ngOnInit(): void {
    this.getAuthenticatedUser();
    this.createForm();
    this.createUserNameForm();
  }

  createUserNameForm() {
    this.userNameForm = this.formBuilder.group({
      userNameControl: new FormControl('')
    });
  }
  
  createForm() {
    this.environmentSettingsForm = this.formBuilder.group({
      environmentMachine: new FormControl(''),
    });
  }

  onChange(e: any) {
    console.log(e.target?.value);
  }

  getAuthenticatedUser() {
    merge()
    .pipe(
      startWith({}),
      switchMap(() => {
        console.log();
        return this.accountService.get();
      }),
    ).subscribe(
      response => {
        console.log(response);
        this.identityUser = response as IdentityUser;
        this.userName = this.identityUser.username;
      },
      error => { 
        console.log(error) 
      },
      () => console.log('getAuthenticatedUser() complete')
    )
  }
}