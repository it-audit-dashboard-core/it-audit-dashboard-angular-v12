import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.css']
})
export class LayoutComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit(): void {

  }
  
  isDashboard(): boolean {
    return this.router.url.length === 1;
  }

  getDashboardClass() {
    return this.isDashboard() ? "col-md-12" : "col-md-8";
  }
}
