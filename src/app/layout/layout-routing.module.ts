import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { DashboardModule } from '../pages/dashboard/dashboard.module';

@NgModule({
  imports: [
    RouterModule.forRoot([
      {
        path: '', loadChildren: () => DashboardModule
      }
    ])
  ],
  exports: [RouterModule],
  declarations: []
})
export class LayoutRoutingModule { }