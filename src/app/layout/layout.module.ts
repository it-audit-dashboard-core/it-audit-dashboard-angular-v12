import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LayoutComponent } from './layout.component';
import { SharedModule } from '@progress/kendo-angular-grid';
import { DataSourcesComponent } from './components/data-sources/data-sources.component';
import { PageTitleComponent } from './components/page-title/page-title.component';
import { LayoutRoutingModule } from './layout-routing.module';
import { PageHeaderComponent } from './components/page-header/page-header.component';
import { DashboardModule } from '../pages/dashboard/dashboard.module';
import { PageFooterComponent } from './components/page-footer/page-footer.component';
import { ApplicationsMenuComponent } from './components/applications-menu/applications-menu.component';
import { SettingsMenuComponent } from './components/settings-menu/settings-menu.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UserApplicationService } from './services/user-application.service';
import { IdentityService } from './services/identity.service';
import { DataInformationService } from './services/data-information.service';
import { IbaUsersModule } from '../pages/iba-users/iba-users.module';
import { AdUsersModule } from '../pages/ad-users/ad-users.module';


@NgModule({
  declarations: [
    LayoutComponent,
    PageHeaderComponent,
    DataSourcesComponent,
    PageFooterComponent,
    ApplicationsMenuComponent,
    SettingsMenuComponent,
    DataSourcesComponent,
    PageTitleComponent
  ],
  imports: [
    CommonModule,
    DashboardModule,
    SharedModule,
    IbaUsersModule,
    AdUsersModule,
    SharedModule,
    LayoutRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [
    UserApplicationService,
    IdentityService,
    DataInformationService
  ]
})
export class LayoutModule { }
