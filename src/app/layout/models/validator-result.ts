export class ValidatedResultDto<TOutput> {
        
    // RESULTDTO
    public resultDto!: TOutput;
    // ERRORS
    public errors: ValidationFailureDto[] = [];
}

export class ValidationFailureDto {

    // PROPERTYNAME
    public propertyName: string = "";
    // ERRORMESSAGE
    public errorMessage: string = "";
    // ATTEMPTEDVALUE
    public attemptedValue: any = null;
    // CUSTOMSTATE
    public customState: any = null;
    // SEVERITY
    public severity!: any;
    // ERRORCODE
    public errorCode: string = "";
}
