import { isInterfaceDeclaration } from "typescript";

export interface UserApplicationDto {
    id:number;
    key: string;
    title: string;
    dataSources: string;
    // /dataSources: DataSourceDto[];
    route: string;
}
6
export interface DataSourceDto {
    name: string;
    modifiedDate: Date;
}

export class DataInformationOutputDto {

    // DATASOURCES
    public dataSources: DataSourceOutputDto[] = [];
}

export class DataSourceOutputDto {

    // MODIFIEDDATE
    public modifiedDate: Date = new Date(0);
    // NAME
    public name: string = "";
}

export interface DataSourceErrorDto {
    message: string;
    alertLevel: number;
}