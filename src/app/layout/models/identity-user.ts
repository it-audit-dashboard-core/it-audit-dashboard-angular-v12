export interface IdentityUser {
    username: string,
    isAuthenticated: boolean,
    authenticationType: string
}