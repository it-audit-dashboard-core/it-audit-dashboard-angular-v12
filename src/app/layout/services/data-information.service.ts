import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import { DataInformationOutputDto } from "../models/user-application";
import { ValidatedResultDto } from "../models/validator-result";

@Injectable({
    providedIn: 'root'
})
export class DataInformationService {
    private uri: string = "https://localhost:44377/api/DataInformation/key?key=";

    constructor(private http: HttpClient) { }

    get(key:string): Observable<ValidatedResultDto<DataInformationOutputDto>> {

        return this.http
            .get<ValidatedResultDto<DataInformationOutputDto>>(this.uri + key)
            .pipe(
                map((response: ValidatedResultDto<DataInformationOutputDto>) => {
                    console.log('get(DataInformationDto) response: ');
                    return response;
                })
            )
    }
}
