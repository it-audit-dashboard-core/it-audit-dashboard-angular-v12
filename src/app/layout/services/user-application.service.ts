import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import { UserApplicationDto } from "../models/user-application";
import { ValidatedResultDto } from "../models/validator-result";


@Injectable({
    providedIn: 'root'
})
export class UserApplicationService {
    private uri: string = "https://localhost:44377/api/UserApplication/";

    constructor(private http: HttpClient) { }

    getListAsync(): Observable<ValidatedResultDto<UserApplicationDto[]>> {

        return this.http
            .get<ValidatedResultDto<UserApplicationDto[]>>(this.uri + "GetListAsync")
            .pipe(
                map((response: ValidatedResultDto<UserApplicationDto[]>) => {
                    console.log('get() response: ')
                    return response;
                })
            )
    }

    getByKeyAsync(key:string): Observable<ValidatedResultDto<UserApplicationDto>> {

        return this.http
            .get<ValidatedResultDto<UserApplicationDto>>(this.uri + "GetByKeyAsync?key=" + key)
            .pipe(
                map((response: ValidatedResultDto<UserApplicationDto>) => {
                    console.log('get() response: ')
                    return response;
                })
            )
    }}
