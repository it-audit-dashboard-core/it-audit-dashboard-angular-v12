import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import { IdentityUser } from "../models/identity-user";


@Injectable({
    providedIn: 'root'
})
export class IdentityService {
    private uri: string = "https://localhost:44377/Account/";

    constructor(private http: HttpClient) { }

    get(): Observable<IdentityUser> {

        return this.http
            .get<IdentityUser>(this.uri + "GetAuthenticatedUser")
            .pipe(
                map((response: IdentityUser) => {
                    console.log('GetAuthenticatedUser() response: ')
                    return response;
                })
            )
    }
}
