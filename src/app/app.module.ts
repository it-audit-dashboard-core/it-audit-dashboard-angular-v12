import { NgModule } from '@angular/core';
import { RouterModule } from "@angular/router";
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { AppComponent } from './app.component';
import { LayoutModule } from './layout/layout.module';
import { WinAuthInterceptor } from './shared/win-auth-interceptor';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SharedModule } from './shared/shared.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    RouterModule,
    HttpClientModule,
    LayoutModule,
    SharedModule,
    NgbModule
   ],
  providers: [
    {
    provide: HTTP_INTERCEPTORS,
    useClass: WinAuthInterceptor,
    multi: true
  }
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule { }
