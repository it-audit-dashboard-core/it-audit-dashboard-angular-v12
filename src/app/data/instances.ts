export const instances = [
    { "label": "Instances", "children": [
        { "label": "AJG", "value": "AJG", "selected": true, "disabled": false }, 
        { "label": "Alesco", "value": "Alesco", "selected": true, "disabled": false }, 
        { "label": "Brasil", "value": "Brasil", "selected": true, "disabled": false }, 
        { "label": "Capsicum Re", "value": "Capsicum Re", "selected": true, "disabled": false }, 
        { "label": "Compass", "value": "Compass", "selected": true, "disabled": false }, 
        { "label": "Execution Re", "value": "Execution Re", "selected": true, "disabled": false }, 
        { "label": "Gallagher Housing", "value": "Gallagher Housing", "selected": true, "disabled": false }, 
        { "label": "Heath", "value": "Heath", "selected": true, "disabled": false }, 
        { "label": "Nordic AJG", "value": "Nordic AJG", "selected": true, "disabled": false }, 
        { "label": "Nordic OIM", "value": "Nordic OIM", "selected": true, "disabled": false }, 
        { "label": "OIM", "value": "OIM", "selected": true, "disabled": false }, 
        { "label": "Pen Underwriting", "value": "Pen Underwriting", "selected": true, "disabled": false }
    ] 
}];