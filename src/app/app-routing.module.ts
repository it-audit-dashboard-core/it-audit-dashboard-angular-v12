import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LayoutComponent } from './layout/layout.component';
import { AdUsersModule } from './pages/ad-users/ad-users.module';
import { DashboardModule } from './pages/dashboard/dashboard.module';
import { IbaUsersModule } from './pages/iba-users/iba-users.module';

const routes: Routes = [
  {
    path: '', component: LayoutComponent,
    children: [
      { path: '', loadChildren: () => DashboardModule },
      { path: 'apps/iba-users', loadChildren: () => IbaUsersModule },
      { path: 'apps/ad-users', loadChildren: () => AdUsersModule }
    ]
  }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }