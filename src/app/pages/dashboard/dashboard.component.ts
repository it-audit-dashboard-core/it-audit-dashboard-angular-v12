import { Component, OnInit } from '@angular/core';
import { merge } from 'rxjs';
import { startWith, switchMap } from 'rxjs/operators';
import { UserApplicationDto } from 'src/app/layout/models/user-application';
import { ValidatedResultDto } from 'src/app/layout/models/validator-result';
import { UserApplicationService } from 'src/app/layout/services/user-application.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  userApplicationsResultDto!: ValidatedResultDto<UserApplicationDto[]>;
  userApplications!: UserApplicationDto[];

  constructor(private userApplicationService: UserApplicationService) { }

  ngOnInit(): void {
    this.getUserApplications();
  }
  
  getUserApplications() {
    merge()
      .pipe(
        startWith({}),
        switchMap(() => {
          return this.userApplicationService.getListAsync();
        }),
      ).subscribe(
        response => {
          this.userApplicationsResultDto = response as ValidatedResultDto<UserApplicationDto[]>;
          console.log(response);
          this.userApplications = response.resultDto;
          console.log(this.userApplications);
        },
        error => { 
          console.log(error) 
        },
        () => console.log('getUserApplications() complete')
      )
  }
}
