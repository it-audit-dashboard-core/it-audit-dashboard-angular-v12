import { Component, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { merge } from 'rxjs';
import { startWith, switchMap } from 'rxjs/operators';
import { IbaUser } from './models/iba-user';
import { IbaUserService } from './services/iba-user-service';

@Component({
  selector: 'app-iba-users',
  templateUrl: './iba-users.component.html',
  styleUrls: ['./iba-users.component.css']
})
export class IbaUsersComponent implements OnInit, OnChanges {
  users!: IbaUser[];
  showSearch!: boolean; 
  
  constructor(private ibaUserService: IbaUserService, private router: Router) { }

  ngOnInit(): void {
  }
  
  ngOnChanges(changes: SimpleChanges): void {
    throw new Error('Method not implemented.');
  }
  getNotification(searchData:FormGroup): void {
    console.log(searchData);
    this.search(searchData.value)
  }

  isDashboard(): boolean {
    return this.router.url.length === 1;
  }
  
  getDashboardClass() {
    return this.isDashboard() ? "col-md-12" : "col-md-8";
  }

  getClass() {
      return !this.users ? 'collapse show' : 'collapse';
  }

  expand(show: boolean) {
    return show ? 'collapse show' : 'collapse';
  }

  search(json: any){
    merge()
      .pipe(
        startWith({}),
        switchMap(() => {
          console.log(json);
          return this.ibaUserService.search(json);
        }),
      ).subscribe(
        response => {
          this.users = response as IbaUser[];
          this.showSearch = false;
          console.log('Users: ' + this.users);
        },
        error => { 
          console.log(error) 
        },
        () => console.log('search() complete')
      )
  }
}
