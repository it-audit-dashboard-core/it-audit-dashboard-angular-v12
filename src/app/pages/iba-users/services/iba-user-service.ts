import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import { IbaUser } from "../models/iba-user";
import { IbaSearchFilterDto, SearchDataDto } from "../models/search-data";


@Injectable({
    providedIn: 'root'
})
export class IbaUserService {
    private uri: string = "https://localhost:44377/api/IbaUsers/";

    constructor(private http: HttpClient) { }

    search(searchData:IbaSearchFilterDto): Observable<IbaUser[]> {

        return this.http
            .post<IbaUser[]>(this.uri  + "Search", searchData)
            .pipe(
                map((response: IbaUser[]) => {
                    console.log('search() response: ')
                    return response;
                })
            )
    }
}
