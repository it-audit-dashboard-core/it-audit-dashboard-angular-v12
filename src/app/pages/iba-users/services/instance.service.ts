import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import { FilterDataResult } from "../models/filter-data";
import { InstanceDto, RoleDto, SecurityDto } from "../models/instance";


@Injectable({
    providedIn: 'root'
})
export class InstanceService {
    private uri: string = "https://localhost:44377/api/IbaUsers/";

    constructor(private http: HttpClient) { }

    getInstancesRolesSecurities(): Observable<FilterDataResult> {
        return this.http
            .get<FilterDataResult>(this.uri + 'GetInstancesRolesSecurities')
            .pipe(
                map((response: FilterDataResult) => {
                    console.log('GetInstancesRolesSecurities() response: ')
                    return response;
                })
            )
    }

    getInstances(): Observable<InstanceDto[]> {

        return this.http
            .get<InstanceDto[]>(this.uri + 'GetInstances')
            .pipe(
                map((response: InstanceDto[]) => {
                    console.log('getInstances() response: ')
                    return response;
                })
            )
    }

    getSecurities(): Observable<SecurityDto[]> {

        return this.http
            .get<SecurityDto[]>(this.uri + 'GetSecurities')
            .pipe(
                map((response: SecurityDto[]) => {
                    console.log('getSecurities() response: ')
                    return response;
                })
            )
    }

    getRoles(): Observable<RoleDto[]> {

        return this.http
            .get<RoleDto[]>(this.uri + 'GetRoles')
            .pipe(
                map((response: RoleDto[]) => {
                    console.log('getRoles() response: ')
                    return response;
                })
            )
    }
}
