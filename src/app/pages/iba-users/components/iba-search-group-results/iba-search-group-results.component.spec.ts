import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IbaSearchGroupResultsComponent } from './iba-search-group-results.component';

describe('IbaSearchGroupResultsComponent', () => {
  let component: IbaSearchGroupResultsComponent;
  let fixture: ComponentFixture<IbaSearchGroupResultsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IbaSearchGroupResultsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IbaSearchGroupResultsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
