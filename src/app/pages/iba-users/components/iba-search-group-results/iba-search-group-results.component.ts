import { Component, OnInit, ViewChild } from "@angular/core";
import { DataBindingDirective } from "@progress/kendo-angular-grid";
import { process } from "@progress/kendo-data-query";
import { images } from "../../models/images";
import { users } from "src/app/data/users";

@Component({
  selector: "app-iba-search-group-results",
  templateUrl: "./iba-search-group-results.component.html",
  styleUrls: ['./iba-search-group-results.component.css']
})
export class IbaSearchGroupResultsComponent implements OnInit {
  @ViewChild(DataBindingDirective) dataBinding!: DataBindingDirective;
  public gridData: any[] = users;
  public gridView: any[] = [];

  public mySelection: string[] = [];

  public ngOnInit(): void {
    this.gridView = this.gridData;
  }

  public onFilter(e: any): void {
    let inputValue = e.value;
    this.gridView = process(this.gridData, {
      filter: {
        logic: "or",
        filters: [
          {
            field: "Name",
            operator: "contains",
            value: inputValue,
          },
          {
            field: "Instance",
            operator: "contains",
            value: inputValue,
          },
          {
            field: "Role",
            operator: "contains",
            value: inputValue,
          },
          {
            field: "Security",
            operator: "contains",
            value: inputValue,
          },
          {
            field: "IsActive",
            operator: "contains",
            value: inputValue,
          },
          {
            field: "UpdatedBy",
            operator: "contains",
            value: inputValue,
          },
          {
            field: "Updated",
            operator: "contains",
            value: inputValue,
          },
          {
            field: "Disabled",
            operator: "contains",
            value: inputValue,
          },
          {
            field: "ServiceDeskRef",
            operator: "contains",
            value: inputValue,
          },
          {
            field: "CreatedDate",
            operator: "contains",
            value: inputValue,
          },
          {
            field: "CreatedRef",
            operator: "contains",
            value: inputValue,
          },
          {
            field: "CreatedUser",
            operator: "contains",
            value: inputValue,
          },
          {
            field: "UserName",
            operator: "contains",
            value: inputValue,
          },
          {
            field: "ADOU",
            operator: "contains",
            value: inputValue,
          },
          {
            field: "AccountStatus",
            operator: "contains",
            value: inputValue,
          },
          {
            field: "Office",
            operator: "contains",
            value: inputValue,
          },
          {
            field: "AD_Department",
            operator: "contains",
            value: inputValue,
          },
          {
            field: "City",
            operator: "contains",
            value: inputValue,
          },
          {
            field: "Manager",
            operator: "contains",
            value: inputValue,
          },
          {
            field: "IBA_Load_Date",
            operator: "contains",
            value: inputValue,
          },
        ],
      },
    }).data;

    this.dataBinding.skip = 0;
  }

  public photoURL(dataItem: any): string {
    const code: string = dataItem.img_id + dataItem.gender;
    const image: any = images;

    return image[code];
  }

  public flagURL(dataItem: any): string {
    const code: string = dataItem.country;
    const image: any = images;

    return image[code];
  }
}