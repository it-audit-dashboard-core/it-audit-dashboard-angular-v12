import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IbaUsersInstanceGroupComponent } from './iba-users-instance-group.component';

describe('IbaUsersInstanceGroupComponent', () => {
  let component: IbaUsersInstanceGroupComponent;
  let fixture: ComponentFixture<IbaUsersInstanceGroupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IbaUsersInstanceGroupComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IbaUsersInstanceGroupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
