import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { merge, of } from 'rxjs';
import { catchError, startWith, switchMap } from 'rxjs/operators';
import { instances } from 'src/app/data/instances';
import { Child, Parent, SelectedChildren } from '../../models/child';
import { FilterDataResult } from '../../models/filter-data';
import { InstanceDto } from '../../models/instance';

import { InstanceService } from '../../services/instance.service';

@Component({
  selector: 'app-iba-users-instance-group',
  templateUrl: './iba-users-instance-group.component.html',
  styleUrls: ['./iba-users-instance-group.component.css']
})
export class IbaUsersInstanceGroupComponent implements OnInit {
  @Input() public ibaSearchForm!: FormGroup

  filterDataResult!: FilterDataResult;
  originalInstances!: Child[];
  originalSecurities!: Child[];
  originalRoles!: Child[];
  instances!: Child[];
  securities!: Child[];
  roles!: Child[];


  selectedSecurities!: SelectedChildren;
  selectedInstances!: SelectedChildren;
  selectedRoles!: SelectedChildren;

  instanceResult!: Parent;
  securityResult!: Parent;
  roleResult!: Parent;

  instanceCount: number = 0;
  securityCount: number = 0;
  roleCount: number = 0;

  hdnInstanceValue: any;
  instanceButtonText: string = '';
  securityButtonText: string = '';
  roleButtonText: string = '';

  constructor(private formBuilder: FormBuilder, private instanceService: InstanceService) { }

  ngOnInit(): void {

    this.getInstancesRolesSecurities();

  }

  getInstancesRolesSecurities() {
    merge()
      .pipe(
        startWith({}),
        switchMap(() => {
          return this.instanceService.getInstancesRolesSecurities();
        }),
      ).subscribe(
        response => {
          this.filterDataResult = response as FilterDataResult;
          this.instanceResult = this.filterDataResult.instanceResult;
          this.securityResult = this.filterDataResult.securityResult;
          this.roleResult = this.filterDataResult.roleResult;

          this.instances  = this.instanceResult.children;
          this.securities = this.securityResult.children;
          this.roles = this.roleResult.children;
          
          this.instanceCount = this.getSelectedChildren(this.instances).count;
          this.securityCount = this.getSelectedChildren(this.securities).count;
          this.roleCount = this.getSelectedChildren(this.roles).count;

          this.instanceButtonText = 'All Selected (' + this.instanceCount + ')';
          this.securityButtonText = 'All Selected (' + this.securityCount + ')';
          this.roleButtonText = 'All Selected (' + this.roleCount + ')';

          console.log(this.instances);
          console.log(this.securities);
          console.log(this.roles);
        },
        error => {
          console.log(error);
        },
        () => console.log('getInstances() complete')
      )
  }

  instanceClicked(e:any) {
    var name = e.target?.name;
    console.log(name);
    var isSelected = e.target?.checked;

    this.instances.forEach(function (value) {
      if (value.label === name) value.selected = isSelected;
    });

    this.instances[0].selected = this.getSelectAllState(this.instances);

    console.log(name);
    if ( name === 'Select All') {
      this.selectAll('instance', isSelected);
    }

    this.securities.forEach(function (value) {
      if (value.label === name) value.selected = isSelected;
    });
  
    let selectedInstance = this.getSelectedChildren(this.instances);
    
    this.instanceButtonText = this.getInstanceButtonText(selectedInstance);

    this.setState(this.securities, name, isSelected);
    this.setState(this.roles, name, isSelected);

    let selectedSecurity = this.getSelectedChildren(this.securities);
    this.securityButtonText = this.getButtonText(selectedSecurity, this.securityCount);
    
    let selectedRole = this.getSelectedChildren(this.roles);
    this.roleButtonText = this.getButtonText(selectedRole, this.roleCount);
  }

  securityClicked(e:any) {
    var name = e.target?.name;
    var isSelected = e.target?.checked;
    
    this.securities.forEach(function (child) {
      if (child.value === name) child.selected = isSelected;
    });

    this.securities[0].selected = this.getSelectAllState(this.securities);

    if ( name === 'Select All') {
      this.selectAll('security', isSelected);
    }

    this.securities.forEach(function (value) {
      if (value.label === name) value.selected = isSelected;
    });

    let selectedSecurity = this.getSelectedChildren(this.securities);
    this.securityButtonText = this.getButtonText(selectedSecurity, this.securityCount);
  }

  
  roleClicked(e:any) {
    var name = e.target?.name;
    var isSelected = e.target?.checked;

    this.roles.forEach(function (child) {
      if (child.value === name) child.selected = isSelected;
    });

    this.roles[0].selected = this.getSelectAllState(this.roles);

    if ( name === 'Select All') {
      this.selectAll('role', isSelected);
    }

    this.roles.forEach(function (value) {
      if (value.label === name) value.selected = isSelected;
    });

    let selectedRole = this.getSelectedChildren(this.roles);
    this.securityButtonText = this.getButtonText(selectedRole, this.roleCount);
  }

  setState(children: Child[], parent: string, selected: boolean) {
    children.forEach(function (child) {
      if (!child.isLabel && child.label !== "Select All") {
        if (child.parent === parent) {
          child.disabled = !selected;
          child.selected = selected;
        }
      }
    });
  }

  getInstanceButtonText(children: SelectedChildren):string{
    if (children.count === this.instanceCount){
      return 'All Selected (' + this.instanceCount + ')';
    }
    
    if (children.count === 0) {
      return 'None Selected';
    }
    if (children.count > 3) {
        return children.count + ' Selected';
    }
    return children.labels.toString();
  }

  getButtonText(children: SelectedChildren, count: number):string{
    console.log(children.count);
    console.log(count);
    if (children.count === count){
      return 'All Selected (' + count + ')';
    }
    
    if (children.count === 0) {
      return 'None Selected';
    }
    if (children.count > 0) {
        return children.count + ' Selected';
    }
    return children.labels.toString();
  }

  getSelectAllState(children:Child[]):boolean {
    let isSelected:boolean = true;
    children.forEach(function (child) {
      if (!child.isLabel && child.label !== "Select All"){
        if (child.selected === false) isSelected = false;
      }
    });

    return isSelected;
  }

  selectAdmins() {
    this.securities.forEach(function (value) {
      value.disabled = false;
      value.selected = (value.label === 'Admin') ? true : false;
    });

    let selectedSecurity = this.getSelectedChildren(this.securities);
    this.securityButtonText = this.getButtonText(selectedSecurity, this.securityCount);
  }

  selectNonAdmins() {
    this.securities.forEach(function (value) {
      value.disabled = false;
      value.selected = (value.label !== 'Admin') ? true : false;
    });

    let selectedSecurity = this.getSelectedChildren(this.securities);
    this.securityButtonText = this.getButtonText(selectedSecurity, this.securityCount);
  }

  resetAdmins() {
    console.log('reset');
    this.getInstancesRolesSecurities();
    // this.setState(this.instances, 'Select All', true);
    // this.setState(this.securities, 'Select All', true);
    // this.setState(this.roles, 'Select All', true);

  }
  
  getSelectedChildren(children:Child[]):SelectedChildren { 
    let count: number = 0;
    let labels: string[] = [];
    let values: string[] = [];
    
    children.forEach(function (child) {
      if (child.selected && !child.isLabel && child.label !== "Select All") {
        count++;
        labels.push(child.label);
        values.push(child.value);
      }
    });

    let selectedChildren = new SelectedChildren();
    selectedChildren.count = count;
    selectedChildren.labels = labels;
    selectedChildren.values = values;
    
    // console.log('count: ' + selectedChildren.count);
    // console.log('labels: ' + selectedChildren.labels);
    // console.log('values: ' + selectedChildren.values);

    return selectedChildren;
  }
  
  selectAll(type: any, isSelected: boolean) {
    //if (isSelected) {
      switch (type) {
        case 'instance':
          this.instances.forEach(function (value) {
            value.selected = isSelected;
          });

          this.securities.forEach(function(child) {
            if (!child.isLabel) {
              child.disabled = !isSelected;
                child.selected = isSelected;
            }
          });
          
          this.roles.forEach(function(child) {
            if (!child.isLabel) {
              child.disabled = !isSelected;
                child.selected = isSelected;
            }
          });
          break;
        case 'security':
          this.securities.forEach(function (value) {
            value.selected = isSelected;
          });
          break;
        case 'role':
          this.roles.forEach(function (value) {
            value.selected = isSelected;
          });
          break;
        default:
          break;
      }
    //}
  }
}
