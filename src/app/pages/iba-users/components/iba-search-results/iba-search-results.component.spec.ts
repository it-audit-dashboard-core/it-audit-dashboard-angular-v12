import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IbaSearchResultsComponent } from './iba-search-results.component';

describe('IbaSearchResultsComponent', () => {
  let component: IbaSearchResultsComponent;
  let fixture: ComponentFixture<IbaSearchResultsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IbaSearchResultsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IbaSearchResultsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
