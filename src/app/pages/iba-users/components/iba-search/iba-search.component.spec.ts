import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IbaSearchComponent } from './iba-search.component';

describe('IbaSearchComponent', () => {
  let component: IbaSearchComponent;
  let fixture: ComponentFixture<IbaSearchComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IbaSearchComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IbaSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
