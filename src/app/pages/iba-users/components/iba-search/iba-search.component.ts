import { Component, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { IbaSearchFilterDto } from '../../models/search-data';
import { IbaUsersInstanceGroupComponent } from '../iba-users-instance-group/iba-users-instance-group.component';

@Component({
  selector: 'app-iba-search',
  templateUrl: './iba-search.component.html',
  styleUrls: ['./iba-search.component.css'],
  providers: [
  ]
})
export class IbaSearchComponent implements OnInit {
  @ViewChild(IbaUsersInstanceGroupComponent) child!:IbaUsersInstanceGroupComponent;
  @Output() notifyParent: EventEmitter<FormGroup> = new EventEmitter();
  searchData:IbaSearchFilterDto = new IbaSearchFilterDto();

  //region FormControls
  ibaSearchForm!: FormGroup;
  name: FormControl | undefined;
  login: FormControl | undefined;
  updatedBy: FormControl | undefined;
  updatedFrom: FormControl | undefined;
  updatedTo: FormControl | undefined;
  createdFrom: FormControl | undefined;
  createdTo: FormControl | undefined;
  createdUser: FormControl | undefined;
  instances: FormControl | undefined;
  securities: FormControl | undefined;
  roles: FormControl | undefined;
  radActive: FormControl | undefined;
  radDisabled: FormControl | undefined;
  hdnInstance: FormControl | undefined;
  radAccountStatus: FormControl | undefined;
  cboInstance: FormControl | undefined;
  cboSecurity: FormControl | undefined;
  cboRole: FormControl | undefined;
  txtTest: FormControl | undefined;
  //endregion
  
  updatedFromValue: Date = new Date();
  updatedToValue: Date = new Date();
  createdFromValue: Date = new Date();
  createdToValue: Date = new Date();

  constructor(private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.createForm(); 
    this.ibaSearchForm.valueChanges.subscribe(newVal => console.log(newVal));
  }

  createForm() {
    this.ibaSearchForm = this.formBuilder.group({
      name: new FormControl(''),
      login: new FormControl(''),
      updatedBy: new FormControl(''),
      updatedFrom: new FormControl(''),
      updatedTo: new FormControl(''),
      createdFrom: new FormControl(''),
      createdTo: new FormControl(''),
      createdUser: new FormControl(''),
      radActive: new FormControl('Active', Validators.required),
      radDisabled: new FormControl('Inactive', Validators.required),
      radAccountStatus: new FormControl('Both', Validators.required),
      hdnInstance: new FormControl(''),
      cboInstance: new FormControl(''),
      cboSecurity: new FormControl(''),
      cboRole: new FormControl(''),
      txtTest: new FormControl('')
    });
  }
   
  submit(){
    
  }

  resetForm(){
    this.createForm();
    this.child.getInstancesRolesSecurities();
  }

  onSubmit() {
    this.notifyParent.emit(this.ibaSearchForm);

    // if (this.ibaSearchForm.valid) {
    //   console.log("Form Submitted!");
    //   //console.log(this.ibaSearchForm.value);
    //   //this.ibaSearchForm.reset();
    //   this.notifyParent.emit(this.ibaSearchForm);
    // } else {
    //   console.log("Form Invalid!");
    // }
  }
}