import { AfterViewInit, Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { merge } from 'rxjs';
import { startWith, switchMap } from 'rxjs/operators';
import { Child, Parent } from '../../models/child';
import { FilterDataResult } from '../../models/filter-data';
import { InstanceService } from '../../services/instance.service';

@Component({
  selector: 'app-iba-users-instances',
  templateUrl: './iba-users-instances.component.html',
  styleUrls: ['./iba-users-instances.component.css']
})
export class IbaUsersInstancesComponent implements OnInit, AfterViewInit {
  @Input() public ibaSearchForm!: FormGroup

  filterDataResult!: FilterDataResult;
  instanceResult!: Parent;
  instances!: Child[];

  instancesText: string = '';
  selectedInstances: string = '';
  hdnInstanceValue: string ='';
  selectedAll: any;
  
  constructor(private formBuilder: FormBuilder, private instanceService: InstanceService) { }
  
  ngAfterViewInit(): void {
    this.getInstancesRolesSecurities();
  }

  ngOnInit(): void {
  }

  getInstancesRolesSecurities() {
    merge()
      .pipe(
        startWith({}),
        switchMap(() => {
          return this.instanceService.getInstancesRolesSecurities();
        }),
      ).subscribe(
        response => {
          this.filterDataResult = response as FilterDataResult;
          this.instanceResult = this.filterDataResult.instanceResult;
          console.log(this.instanceResult);

          // this.securityResult = this.filterDataResult.securityResult;
          // this.roleResult = this.filterDataResult.roleResult;

          this.instances = this.instanceResult.children;
          this.instancesText = 'All selected (12)';
          this.selectedInstances = 'AJG, Alesco, Brasil, Capsicum Re, Compass, Execution Re, Gallagher Housing, Heath, Nordic AJG, Nordic OIM, OIM, Pen Underwriting';
          // this.securities = this.securityResult.children;
          // this.roles = this.roleResult.children;

        },
        error => {
          console.log(error);
        },
        () => console.log('getInstances() complete')
      )
  }

  instanceClicked(e:any) {
    //this.instancesText = 'changed';
    var name = e.target?.name; 
    var isSelected = e.target?.checked;
    //this.instances.
  }

  selectAll() {
    for (var i = 0; i < this.instances.length; i++) {
      this.instances[i].selected = true;
    }
  }
  checkIfAllSelected() {
    this.selectedAll = this.instances.every(function(item:any) {
        return item.selected == true;
      })
  }

  selectAdmins() {
    console.log("selectAdmins()");
  }

  selectNonAdmins() {
    console.log("selectNonAdmins()");
  }
}
