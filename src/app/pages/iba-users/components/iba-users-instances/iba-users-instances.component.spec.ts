import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IbaUsersInstancesComponent } from './iba-users-instances.component';

describe('IbaUsersInstancesComponent', () => {
  let component: IbaUsersInstancesComponent;
  let fixture: ComponentFixture<IbaUsersInstancesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IbaUsersInstancesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IbaUsersInstancesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
