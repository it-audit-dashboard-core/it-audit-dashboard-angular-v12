import { count } from "@progress/kendo-data-query/dist/npm/array.operators";

export interface Parent {
    name:     string;
    children: Child[];
    count:    number;
}
export class Child {
    label: string = '';
    value: string = '';
    selected: boolean = false;
    disabled: boolean  = false;
    parent: string = '';
    isLabel: boolean = false;
}

export class SelectedChildren {
    count: number = 0;
    values: string[] = [];
    labels: string[] = [];
    
}