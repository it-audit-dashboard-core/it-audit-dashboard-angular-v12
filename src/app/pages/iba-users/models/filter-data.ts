import { Parent } from "./child";

export interface FilterDataResult {
    instanceResult: Parent;
    securityResult: Parent;
    roleResult: Parent;
}