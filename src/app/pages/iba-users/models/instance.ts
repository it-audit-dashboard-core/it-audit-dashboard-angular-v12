import { Child } from "./child";

export class InstanceDto {
    label: string | undefined;
    value: string | undefined;
    selected: boolean | undefined;
    disabled: boolean | undefined;
    children!: Child[];
}

export class SecurityDto {
    label: string = "";
    children: Child[] = [];
}

export class RoleDto {
    constructor(label: string) {
        this.label = label;
    }

    label: string = "";
    children: Child[] = [];
}

