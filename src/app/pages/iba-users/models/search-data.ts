export class IbaSearchFilterDto {
    name!:string | null;
    instances!: string[];
    securities!: string[];
    roles!: string[];
    login!:string |  null;
    active!: string;
    both!: string;
    updatedBy!: string | null;
    updatedFrom!: string | null;
    dpdatedTo!:string |  null;
    disabled!: string;
    serviceDeskRef!: string | null;
    createdDateFrom!: string | null;
    createdDateTo!:string |  null;
    createdRef!: string | null;
    createdUser!: string | null;
    userName!: string | null;
    adou!: string | null;
    accountStatus!: string;
    search!: string | null;
    updatedFromDate!: string | null;
    updatedToDate!: string | null;
    createdFromDate!:string |  null;
    createdToDate!: string | null;
    activeFlag!: number;
    disabledFlag!: number;
    accountStatusFlag!: number;
}

export class SearchDataDto {
    name!:string | null;
    instances!: string[];
    securities!: string[];
    roles!: string[];
    login!:string |  null;
    active!: string;
    updatedBy!: string | null;
    updatedFrom!: string | null;
    dpdatedTo!:string |  null;
    disabled!: string;
    serviceDeskRef!: string | null;
    createdDateFrom!: string | null;
    createdDateTo!:string |  null;
    createdRef!: string | null;
    createdUser!: string | null;
    userName!: string | null;
    adou!: string | null;
    accountStatus!: string;
    search!: string | null;
    updatedFromDate!: string | null;
    updatedToDate!: string | null;
    createdFromDate!:string |  null;
    createdToDate!: string | null;
    activeFlag!: number;
    disabledFlag!: number;
    accountStatusFlag!: number;
}
