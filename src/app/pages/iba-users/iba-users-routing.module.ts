import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IbaUsersComponent } from './iba-users.component';

const routes: Routes = [
  { path: '', component: IbaUsersComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class IbaUsersRoutingModule { }