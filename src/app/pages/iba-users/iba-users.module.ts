import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ExcelModule, GridModule, PDFModule } from '@progress/kendo-angular-grid';
import { IbaUsersComponent } from './iba-users.component';
import { IbaSearchComponent } from './components/iba-search/iba-search.component';
import { IbaUserService } from './services/iba-user-service';
import { InstanceService } from './services/instance.service';
import { IbaUsersInstanceGroupComponent } from './components/iba-users-instance-group/iba-users-instance-group.component';
import { LabelModule } from '@progress/kendo-angular-label';
import { NumericTextBoxModule } from '@progress/kendo-angular-inputs';
import { DropDownListModule, DropDownsModule } from '@progress/kendo-angular-dropdowns';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { IbaUsersInstancesComponent } from './components/iba-users-instances/iba-users-instances.component';
import { DateInputsModule } from '@progress/kendo-angular-dateinputs';
import { ButtonsModule } from '@progress/kendo-angular-buttons';
import { IbaSearchResultsComponent } from './components/iba-search-results/iba-search-results.component';
import { IbaSearchGroupResultsComponent } from './components/iba-search-group-results/iba-search-group-results.component';
import { NotificationsComponent } from 'src/app/shared/components/notifications/notifications.component';

const routes: Routes = [{
  path: '',
  component: IbaUsersComponent
}];


@NgModule({
  declarations: [
    IbaSearchComponent,
    IbaUsersInstanceGroupComponent,
    IbaUsersComponent,
    IbaUsersInstancesComponent,
    IbaSearchResultsComponent,
    IbaSearchGroupResultsComponent,
    NotificationsComponent
  ],
  imports: [
    FormsModule,
    CommonModule,
    ReactiveFormsModule,
    NgbModule,
    GridModule,
    ExcelModule,
    PDFModule,
    LabelModule,
    DropDownListModule,
    DropDownsModule,
    NumericTextBoxModule,
    DateInputsModule,
    ButtonsModule,
    RouterModule.forChild(routes)
  ],
  providers:[
    IbaUserService,
    InstanceService
  ]
})
export class IbaUsersModule { }
