import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdUsersComponent } from './ad-users.component';
import { AdSearchResultsComponent } from './components/ad-search-results/ad-search-results.component';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ExcelModule, GridModule, PDFModule } from '@progress/kendo-angular-grid';
import { LabelModule } from '@progress/kendo-angular-label';
import { NumericTextBoxModule } from '@progress/kendo-angular-inputs';
import { DropDownListModule, DropDownsModule } from '@progress/kendo-angular-dropdowns';
import { DateInputsModule } from '@progress/kendo-angular-dateinputs';
import { ButtonsModule } from '@progress/kendo-angular-buttons';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

const routes: Routes = [{
  path: '',
  component: AdUsersComponent
}];


@NgModule({
  declarations: [
    AdUsersComponent,
    AdSearchResultsComponent
  ],
  imports: [
    FormsModule,
    CommonModule,
    ReactiveFormsModule,
    NgbModule,
    GridModule,
    ExcelModule,
    PDFModule,
    LabelModule,
    DropDownListModule,
    DropDownsModule,
    NumericTextBoxModule,
    DateInputsModule,
    ButtonsModule,
    RouterModule.forChild(routes)
  ],
  providers: [

  ]
})
export class AdUsersModule { }
