import { Component, Input, OnChanges, OnInit, SimpleChanges, ViewChild } from "@angular/core";
import { DataBindingDirective, PageChangeEvent } from "@progress/kendo-angular-grid";
import { process } from "@progress/kendo-data-query";
import { users } from "src/app/data/users";

@Component({
  selector: 'app-ad-search-results',
  templateUrl: './ad-search-results.component.html',
  styleUrls: ['./ad-search-results.component.css']
})
export class AdSearchResultsComponent implements OnInit {
  @Input() users!: any[];
  @ViewChild(DataBindingDirective) dataBinding!: DataBindingDirective;
  public gridData: any[] = users;
  public gridView: any[] = [];

  public mySelection: string[] = [];

  public buttonCount: number =  10;
  public info: boolean = true;
  public type: 'numeric' | 'input' = 'numeric';
  public pageSize: number = 5;
  public skip: number = 0;
  public pageSizes: boolean = true;
  public previousNext: boolean = true;

  public ngOnInit(): void {
    this.gridData = this.users;
    this.gridView = this.gridData;
  }

  ngOnChanges(changes: SimpleChanges): void {
    console.log('ngOnChanges called...');
    //this.gridData = this.users;
    this.gridView = this.gridData;
  }

  public pageChange({ skip, take }: PageChangeEvent): void {
    this.skip = skip;
    this.pageSize = take;
  }

  public onFilter(e: any): void {
    let inputValue = e.value;
    this.gridView = process(this.gridData, {
      filter: {
        logic: "or",
        filters: [
          {
            field: "name",
            operator: "contains",
            value: inputValue,
          },
          {
            field: "instance",
            operator: "contains",
            value: inputValue,
          },
          {
            field: "role",
            operator: "contains",
            value: inputValue,
          },
          {
            field: "security",
            operator: "contains",
            value: inputValue,
          },
          // {
          //   field: "IsActive",
          //   operator: "contains",
          //   value: inputValue,
          // },
          {
            field: "updatedBy",
            operator: "contains",
            value: inputValue,
          },
          // {
          //   field: "Updated",
          //   operator: "contains",
          //   value: inputValue,
          // },
          // {
          //   field: "Disabled",
          //   operator: "contains",
          //   value: inputValue,
          // },
          {
            field: "serviceDeskRef",
            operator: "contains",
            value: inputValue,
          },
          // {
          //   field: "CreatedDate",
          //   operator: "contains",
          //   value: inputValue,
          // },
          {
            field: "createdRef",
            operator: "contains",
            value: inputValue,
          },
          {
            field: "createdUser",
            operator: "contains",
            value: inputValue,
          },
          {
            field: "userName",
            operator: "contains",
            value: inputValue,
          },
          {
            field: "adou",
            operator: "contains",
            value: inputValue,
          },
          {
            field: "accountStatus",
            operator: "contains",
            value: inputValue,
          },
          {
            field: "office",
            operator: "contains",
            value: inputValue,
          },
          {
            field: "ad_department",
            operator: "contains",
            value: inputValue,
          },
          {
            field: "city",
            operator: "contains",
            value: inputValue,
          },
          {
            field: "manager",
            operator: "contains",
            value: inputValue,
          },
          // {
          //   field: "IBA_Load_Date",
          //   operator: "contains",
          //   value: inputValue,
          // },
        ],
      },
    }).data;

    this.dataBinding.skip = 0;
  }
}