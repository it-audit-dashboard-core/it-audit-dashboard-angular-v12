import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdSearchResultsComponent } from './ad-search-results.component';

describe('AdSearchResultsComponent', () => {
  let component: AdSearchResultsComponent;
  let fixture: ComponentFixture<AdSearchResultsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdSearchResultsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdSearchResultsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
