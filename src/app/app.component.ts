import { Component, OnInit } from '@angular/core';
import { merge, of } from 'rxjs';
import { catchError, startWith, switchMap } from 'rxjs/operators';
import { IdentityUser } from './layout/models/identity-user';
import { IdentityService } from './layout/services/identity.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  identityUser!: IdentityUser
  username: string = "";
  isAuthenticated: boolean = false;

  constructor(private identityService: IdentityService) {

  }

  ngOnInit(): void {
    this.getUser();
  }

  getUser() {
    merge()
      .pipe(
        startWith({}),
        switchMap(() => {
          return this.identityService.get();
        }),
        catchError(() => {
          console.log('getUser() error: ')
          return of();
        })
      ).subscribe(response => {
        this.identityUser = response as IdentityUser;

        this.username = this.identityUser.username;
        this.isAuthenticated = this.identityUser.isAuthenticated;
        console.log('this.username: ' + this.username);
        console.log('this.isAuthenticated: ' + this.isAuthenticated);

      })
  }
}
