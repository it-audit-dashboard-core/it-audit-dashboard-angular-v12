# ------------------------------------------------------------------
# Site Warm Up script
# ------------------------------------------------------------------
param(
        [Parameter(Mandatory=$false)][string]$TargetURL_OnlyForSpecificSiteOrWebApp,
        [ValidateSet("SpecificSite","SpecificSPWebApplication","AllSPWebApplications","AllSPWebApplicationsAndCentralAdmin")][Parameter(Mandatory=$true)]$Scope
)

#$TargetURL_OnlyForSpecificSiteOrWebApp = "http://Claimsukdev.emea.ajgco.com/"
#$Scope = "SpecificSite"

# Only Load SharePoint PS-Snapin if not warming up a specific site.
if(-not ($Scope.ToLower() -eq  "specificsite"))
{
    if ((Get-PSSnapin -Name "Microsoft.sharepoint.powershell" -ErrorAction SilentlyContinue) -eq $null)
    {
        Add-PSSnapin "Microsoft.sharepoint.powershell";
    }
}


$cred = [System.Net.CredentialCache]::DefaultCredentials

function get-webpage([string]$url,[System.Net.NetworkCredential]$cred=$null)
{
   $wc = new-object net.webclient
   if($cred -eq $null)
   {
     $cred = [System.Net.CredentialCache]::DefaultCredentials;
   }
   $wc.credentials = $cred;
   return $wc.DownloadString($url);
}

if(($Scope.ToLower() -eq  "specificsite") -or ($Scope.ToLower() -eq  "specificspwebapplication"))
{
    if(-not $TargetURL_OnlyForSpecificSiteOrWebApp)
    {
        throw ("TargetURL_OnlyForSpecificSiteOrWebApp is a required field when Scope is 'SpecificSite' or 'SpecificSPWebApplication'")
    }
}

if($Scope.ToLower() -eq "specificsite")
{
    Write-Output "Warming up Specific Site..."
    Write-Output $TargetURL_OnlyForSpecificSiteOrWebApp
    $html=get-webpage -url $TargetURL_OnlyForSpecificSiteOrWebApp -cred $cred
}
elseif($Scope.ToLower() -eq "specificspwebapplication")
{
    $apps = Get-SPWebApplication $TargetURL_OnlyForSpecificSiteOrWebApp -ErrorAction SilentlyContinue
    
    if($apps)
    {
        Write-Output "Warming up Specific SP Web Application sites..."
        foreach ($app in $apps) {
            $sites = Get-SPSite -WebApplication $app.url -Limit ALL -ErrorAction SilentlyContinue -WarningAction SilentlyContinue
            foreach ($site in $sites) {
                Write-Output $site.Url
                $html=get-webpage -url $site.Url -cred $cred
            }
        }
    }
    else
    {
        throw "The web Application was not found"
    }
}
elseif($Scope.ToLower() -eq "allspwebapplications")
{
    $apps = Get-SPWebApplication
    Write-Output "Warming up All SP Web Applications sites..."
    
    foreach ($app in $apps) {
        $sites = Get-SPSite -WebApplication $app.url -Limit ALL -ErrorAction SilentlyContinue -WarningAction SilentlyContinue
        foreach ($site in $sites) {
            Write-Output $site.Url
            $html=get-webpage -url $site.Url -cred $cred
        }
    }
}
else
{
    $apps = Get-SPWebApplication -IncludeCentralAdministration
    Write-Output "Warming up All All SP Web Applications And Central Admin sites..."
    
    foreach ($app in $apps) {
        $sites = Get-SPSite -WebApplication $app.url -Limit ALL -ErrorAction SilentlyContinue -WarningAction SilentlyContinue
        foreach ($site in $sites) {
            Write-Output $site.Url
            $html=get-webpage -url $site.Url -cred $cred;
        }
    }  
}