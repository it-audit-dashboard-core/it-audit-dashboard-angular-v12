﻿# ------------------------------------------------------------------
# ---------------Copy Directory-------------------------------------
# ------------------------------------------------------------------
PARAM(
        [Parameter(Mandatory=$true)][string]$SourcePath,
        [Parameter(Mandatory=$true)][string]$DestinationPath,
        [ValidateSet("true", "false")][string]$Recurse
    )

#[string]$SourcePath = "C:\Users\afooks\Source\Repos\cats-ews\AJG.CATS.ExchangeWebService\AJG.CATS.ExchangeWebService\bin\Debug\AJG_CAST_EWS_DeploymentPackage\Code"
#[string]$DestinationPath = "C:\Users\afooks\Downloads\somefolder\child\child2\child3"
#[string]$Recurse = "true"

$recurseBool = [System.Convert]::ToBoolean($Recurse);

New-Item -Path $DestinationPath -ItemType "directory" -ErrorAction SilentlyContinue

if($recurseBool)
{
    $DirectoryItems = Get-ChildItem -Path $SourcePath -Recurse
}
else
{
    $DirectoryItems = Get-ChildItem -Path $SourcePath
}

function Do-Work{
    $DirectoryItems | Copy-Item -ErrorAction SilentlyContinue -Destination {   
    if ($_.PSIsContainer) {               
        Join-Path $DestinationPath $_.Parent.FullName.Substring($SourcePath.length)
    } 
    else 
    {               
        Join-Path $DestinationPath $_.FullName.Substring($SourcePath.length)
    }            
 } -Force
}

try
{
    Do-Work
} catch{}
Do-Work

Write-Output "Copy files has completed"