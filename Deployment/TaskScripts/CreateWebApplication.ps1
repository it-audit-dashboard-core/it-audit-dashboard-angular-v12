param($NewSiteUrl, $ContentDatabase, $WebSiteName, $WebsiteDesc, $Template, $PrimaryLogin, $PrimaryLoginDisplay, $PrimaryLoginEmail)

if ( (Get-PSSnapin -Name "Microsoft.sharepoint.powershell" -ErrorAction SilentlyContinue) -eq $null )
{
    Add-PSSnapin "Microsoft.sharepoint.powershell";
}

Import-Module WebAdministration -ErrorAction SilentlyContinue

$WebApplicationURL = "http://themba"

$WebApplicationName = "themba we site"

$ContentDatabase = "themba_ContentDB"

$ApplicationPoolDisplayName = "themba app pool"

$ApplicationPoolIdentity = "mscrm2011\Administrator"

$ApplicationPoolPassword = "P@ssw0rd"

$PortalSuperReader = "mscrm2011\Administrator"

$PortalSuperUser = "mscrm2011\Administrator"

$AppPoolStatus = $False

if(get-spwebapplication $WebApplicationURL -ErrorAction SilentlyContinue)
{
    Write-Output "Aborting: Web Application $WebApplicationURL Already Exists" -ForegroundColor Red
    sleep 5
}
else
{   
    if(Get-SPServiceApplicationPool $ApplicationPoolDisplayName -ErrorAction SilentlyContinue)
    {
        Set-Variable -Name AppPoolStatus -Value "IsSharePoint" -scope "script"
    }
    else
    {
        if((Test-Path IIS:\AppPools\$ApplicationPoolDisplayName).tostring() -eq "True")
        {
           Set-Variable -Name AppPoolStatus -Value "IsNotSharePoint" -scope "script"
        }
    }
        
    if($AppPoolStatus -eq "IsNotSharePoint")
    {
        Write-Output "Aborting: Application Pool $ApplicationPoolDisplayName already exists on the server and is not a SharePoint Application Pool" -ForegroundColor Red
    }
    elseif($AppPoolStatus -eq "IsSharePoint")
    {
        if($WebApplicationURL.StartsWith("http://"))
        {
            $HostHeader = $WebApplicationURL.Substring(7)
            $HTTPPort = "8080"
        }
        elseif($WebApplicationURL.StartsWith("https://"))
        {
            $HostHeader = $WebApplicationURL.Substring(8)
            $HTTPPort = "443"
        }
        
        Set-Variable -Name AppPool -Value (Get-SPServiceApplicationPool $ApplicationPoolDisplayName) -scope "script"
        
        $WebApp = New-SPWebApplication -ApplicationPool $ApplicationPoolDisplayName -Name $WebApplicationName -url $WebApplicationURL -port $HTTPPort -DatabaseName $ContentDatabase -HostHeader $hostHeader
        
        $WebApp.Properties["portalsuperuseraccount"] = $PortalSuperUser
        $WebApp.Properties["portalsuperreaderaccount"] = $PortalSuperReader
        
        $SuperUserPolicy = $WebApp.Policies.Add($PortalSuperUser, "Portal Super User Account")

        $SuperUserPolicy.PolicyRoleBindings.Add($WebApp.PolicyRoles.GetSpecialRole([Microsoft.SharePoint.Administration.SPPolicyRoleType]::FullControl))

        $SuperReaderPolicy = $WebApp.Policies.Add($PortalSuperReader, "Portal Super Reader Account")

        $SuperReaderPolicy.PolicyRoleBindings.Add($WebApp.PolicyRoles.GetSpecialRole([Microsoft.SharePoint.Administration.SPPolicyRoleType]::FullRead))
        
        $WebApp.update()

    }
    else
    {
        if(Get-SPManagedAccount $ApplicationPoolIdentity)
        {
            Set-Variable -Name AppPoolManagedAccount -Value (Get-SPManagedAccount $ApplicationPoolIdentity | select username) -scope "Script"
            Set-Variable -Name AppPool -Value (New-SPServiceApplicationPool -Name $ApplicationPoolDisplayName -Account $ApplicationPoolIdentity) -scope "Script"
        }
        else
        {
            $AppPoolCredentials = New-Object System.Management.Automation.PSCredential $ApplicationPoolIdentity, (ConvertTo-SecureString $ApplicationPoolPassword -AsPlainText -Force)
            Set-Variable -Name AppPoolManagedAccount -Value (New-SPManagedAccount -Credential $AppPoolCredentials) -scope "Script"
            
            Set-Variable -Name AppPool -Value (New-SPServiceApplicationPool -Name $ApplicationPoolDisplayName -Account (get-spmanagedaccount $ApplicationPoolIdentity)) -scope "Script"
            
        }
        if($WebApplicationURL.StartsWith("http://"))
        {
            $HostHeader = $WebApplicationURL.Substring(7)
            $HTTPPort = "8080"
        }
        elseif($WebApplicationURL.StartsWith("https://"))
        {
            $HostHeader = $WebApplicationURL.Substring(8)
            $HTTPPort = "443"
        }
        
        $WebApp = New-SPWebApplication -ApplicationPool $AppPool.Name -ApplicationPoolAccount $AppPoolManagedAccount.Username -Name $WebApplicationName -url $WebApplicationURL -port $HTTPPort -DatabaseName $ContentDatabase -HostHeader $hostHeader
        
        $WebApp.Properties["portalsuperuseraccount"] = $PortalSuperUser
        $WebApp.Properties["portalsuperreaderaccount"] = $PortalSuperReader

        $SuperUserPolicy = $WebApp.Policies.Add($PortalSuperUser, "Portal Super User Account")

        $SuperUserPolicy.PolicyRoleBindings.Add($WebApp.PolicyRoles.GetSpecialRole([Microsoft.SharePoint.Administration.SPPolicyRoleType]::FullControl))

        $SuperReaderPolicy = $WebApp.Policies.Add($PortalSuperReader, "Portal Super Reader Account")

        $SuperReaderPolicy.PolicyRoleBindings.Add($WebApp.PolicyRoles.GetSpecialRole([Microsoft.SharePoint.Administration.SPPolicyRoleType]::FullRead))
        
        $WebApp.update() 
    }
}