﻿param(  [Parameter(Mandatory=$true)][string]$FilePath,
        [Parameter(Mandatory=$true)][string]$ReplacementToken,
        [Parameter(Mandatory=$true)][string]$ReplacementText)

[string]$FileContent = Get-Content $FilePath 
$FileContent.Replace($ReplacementToken, $ReplacementText) | Set-Content $FilePath