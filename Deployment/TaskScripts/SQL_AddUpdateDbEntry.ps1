# ------------------------------------------------------------------
# ------- SQL Add \ update db entry ---------
# ------------------------------------------------------------------
param(  [Parameter(Mandatory=$true)][string]$ConnectionString,
        [Parameter(Mandatory=$true)][string]$CheckQuery,
        [Parameter(Mandatory=$true)][string]$InsertQuery,
        [Parameter(Mandatory=$true)][string]$UpdateQuery
       )

#$ConnectionString = "Data Source=EMEUKHC4DB01VT\TLS16LT1,63126;Initial Catalog=SOX_Automation_Core;Integrated Security=True;Pooling=False;MultipleActiveResultSets=true"
#$CheckQuery = "select * from [SOX_Automation_Core].[dbo].[t_EWS_AppSetting] where Name = 'test2'"
#$UpdateQuery = "Update [SOX_Automation_Core].[dbo].[t_EWS_AppSetting] Set [Name]='test1', [Value]='test1' where [Name]='test2'"
#$InsertQuery = "insert into [SOX_Automation_Core].[dbo].[t_EWS_AppSetting] ([Name],[Value],[Description]) Values ('test3','test3','c')"

try 
{
    # Create and open a database connection
    $sqlConnection = new-object System.Data.SqlClient.SqlConnection $ConnectionString
    $sqlConnection.Open()

    # Create a command object
    $sqlCommand = $sqlConnection.CreateCommand()
    $sqlCommand.CommandText = $CheckQuery

    $sqlReader = $sqlCommand.ExecuteReader()

    $entryExists = $false

    if($sqlReader)
    {
        while ($sqlReader.Read())
        {
            $entryExists = $true
        }
    }

    if($entryExists)
    {
        # Execute the update command
        $sqlCommand = $sqlConnection.CreateCommand()
        $sqlCommand.CommandText = $UpdateQuery
        $sqlReader = $sqlCommand.ExecuteReader()
    }
    else
    {
        # execute the insert command
        $sqlCommand = $sqlConnection.CreateCommand()
        $sqlCommand.CommandText = $InsertQuery
        $sqlReader = $sqlCommand.ExecuteReader()
    }
}
catch
{
    Write-Warning "Add \ Update db entry: An Exception Occured: $($Error[0])"
    throw ("Add \ Update db entry: An Exception Occured: $($Error[0])")
}