# ------------------------------------------------------------------
# -------Set Welcome Page-------------------------------------------
# ------------------------------------------------------------------
param(  [Parameter(Mandatory=$true)][string]$WebUrl,
        [Parameter(Mandatory=$true)][string]$WelcomePage)

if ( (Get-PSSnapin -Name "Microsoft.sharepoint.powershell" -ErrorAction SilentlyContinue) -eq $null )
{
    Add-PSSnapin "Microsoft.sharepoint.powershell";
}

$web = Get-SPWeb $WebUrl

if($web -ne $null)
{
    $rootFolder=$web.RootFolder;
    $rootFolder.WelcomePage=$WelcomePage;
    $rootFolder.Update();
    $web.Update();
    Write-Output(‘Welcome Page changed successfully’);
}
else
{
    throw "The Site does not exist: $web"
}
$web.Dispose();