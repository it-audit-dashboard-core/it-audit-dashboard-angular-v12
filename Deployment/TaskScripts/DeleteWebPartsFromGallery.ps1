# ------------------------------------------------------------------
# -------Delete WebParts from gallery-------------------------------
# ------------------------------------------------------------------
param(  [Parameter(Mandatory=$true)][string]$WebUrl,
        [Parameter(Mandatory=$true)][string]$GroupName)

if ( (Get-PSSnapin -Name "Microsoft.sharepoint.powershell" -ErrorAction SilentlyContinue) -eq $null )
{
    Add-PSSnapin "Microsoft.sharepoint.powershell";
}

$web = Get-SPWeb $WebUrl

if($web -ne $null)
{
    $webPartGallery = $web.Lists["Web Part Gallery"];
    foreach ($item in $webPartGallery.Items)
    {
        if($item["Group"] -eq $GroupName)
        {
            $webPartGallery.GetItemById($item.ID).Delete();
            $itemName = $item.Name
            Write-Output("WebPart: $itemName, deleted successfully");
        }
    }
    Write-Output("WebPart(s) with group name: $GroupName, deleted successfully");
}
else
{
    throw "The site does not exist: $web"
}
$web.Dispose();