# ------------------------------------------------------------------
# -------Uninstall Solution-------------------------------------------
# ------------------------------------------------------------------
param(  [Parameter(Mandatory=$true)][string]$SolutionId,
        [Parameter(Mandatory=$true)][string]$SolutionName)

if ( (Get-PSSnapin -Name "Microsoft.sharepoint.powershell" -ErrorAction SilentlyContinue) -eq $null )
{
    Add-PSSnapin "Microsoft.sharepoint.powershell";
}

function WaitForJobToFinish([string]$SolutionFileName)
{
    Start-Sleep -Seconds 2
    $JobName = "*solution-deployment*$SolutionFileName*"
    Write-Output("Job Nameis: $JobName");
    
    # Check that the Get-TimerJob command is working.....
    $AllTimerJobs = Get-SPTimerJob
    if(-not $AllTimerJobs)
    {
        throw "The Get-SPTimerJob query returned zero results....there could be a problem with the timer service on this farm..."
    }
    
    $job = Get-SPTimerJob | ?{ $_.Name -like $JobName }
    if ($job -eq $null)
    {
        Write-Output('Timer job not found');
    }
    else
    {
        $JobFullName = $job.Name
        Write-Output("Waiting to finish job $JobFullName");
        
        while ((Get-SPTimerJob $JobFullName) -ne $null)
        {
            Write-Output(".");
            Start-Sleep -Seconds 2
        }
        Write-Output("Finished waiting for job..");
    }
}

try
{
	$solution = Get-SPSolution -Identity $SolutionId
	if($solution.ContainsWebApplicationResource)
	{
		Uninstall-SPSolution -Identity $SolutionId -allwebapplications -Confirm:$false
	}
	else
	{
		Uninstall-SPSolution -Identity $SolutionId -Confirm:$false
	}
    Write-Output("Uninstall solution");
    
    Write-Output("Waiting for job to finish");
    WaitForJobToFinish $SolutionName
}catch{ Write-Output("Solution not found"); }
try
{
    Write-Output("Removing solution");
    Remove-SPSolution –Identity $SolutionId -Confirm:$false
}catch{throw "There was an error in Removing the solution with Exception: $_"}