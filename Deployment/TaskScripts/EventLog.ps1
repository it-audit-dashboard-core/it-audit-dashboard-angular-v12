﻿# ------------------------------------------------------------------
# -------Event Log Functions----------------------------------------
# ------------------------------------------------------------------
param(
        [ValidateSet("Create", "RemoveEventLog", "RemoveEventSource", "AddEntry")][Parameter(Mandatory=$true)][string]$EventLogAction,
        [Parameter(Mandatory=$true)][string]$LogName,
        [Parameter(Mandatory=$true)][string]$Source,
        [ValidateSet("Information", "Warning", "Error")][Parameter(Mandatory=$false)][string]$EventType,
        [Parameter(Mandatory=$false)][string]$LogText,
        [Parameter(Mandatory=$false)][int]$EventId
    )

#[string]$EventLogAction = "AddEntry"
#[string]$EventType = "Information"
#[int]$EventId = "1"
#[string]$LogName = "AJG-Claims-Workflow"
#[string]$Source = "Exchange Notification Service"
#[string]$LogText = "Created log as part of powershell deployment of claims workflow solution"

# Ensure event type is defined
if(-not $EventType)
{
    $EventType = "Information"
}
# Ensure Event id is defined.
if(-not $EventId)
{
    $EventId = 0
}
# Ensure logtext is defined.
if(-not $LogText)
{
    $LogText = ""
}

if($EventLogAction.ToLower() -eq "Create")
{
    # Check if LogSource exists
    if(-not [System.Diagnostics.EventLog]::SourceExists($Source))
    {
        # Create the log
        New-EventLog -LogName $LogName -Source $Source
        # Now Add a log entry.
        Write-EventLog -LogName $LogName -Source $Source -Message "New Log Created By Event Viewer powershell script." -EntryType $EventType -EventId $EventId
        Write-Output "New Log Created By Event Viewer powershell script."
    }
    else
    {
        Write-EventLog -LogName $LogName -Source $Source -Message "Attempted to Create log during claims workflow deployment, but already existed" -EntryType $EventType -EventId $EventId
        Write-Output "Log Already Exists"
    }
}
elseif($EventLogAction.ToLower() -eq "RemoveEventLog")
{
    # Check if exists
    if([System.Diagnostics.EventLog]::Exists($LogName))
    {
        Remove-EventLog -LogName $LogName
        Write-Output "'$LogName' succesffully removed"
    }
    else
    {
        Write-Output "'$LogName' does not exist"
    }
}
elseif($EventLogAction.ToLower() -eq "RemoveEventSource")
{
    if([System.Diagnostics.EventLog]::SourceExists($Source))
    {
        [System.Diagnostics.EventLog]::DeleteEventSource($Source)
        Write-Output "'$Source' succesffully removed"
    }
    else
    {
        Write-Output "'$Source' does not exist"
    }
}
elseif($EventLogAction.ToLower() -eq "AddEntry")
{
    # if Doesnt exist
    if(-not [System.Diagnostics.EventLog]::SourceExists($Source))
    {
        # Add the log
        New-EventLog -LogName $LogName -Source $Source
    }
    # now add the requested log.
    Write-EventLog -LogName $LogName -Source $Source -Message $LogText -EntryType "Information" -EventId $EventId
    Write-Output "Log entry Successfully written"
}