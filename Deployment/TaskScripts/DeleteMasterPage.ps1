# ------------------------------------------------------------------
# -------Delete Master Page-----------------------------------------
# ------------------------------------------------------------------
param(  [Parameter(Mandatory=$true)][string]$WebUrl,
        [Parameter(Mandatory=$true)][string]$MasterPageName)

if ( (Get-PSSnapin -Name "Microsoft.sharepoint.powershell" -ErrorAction SilentlyContinue) -eq $null )
{
    Add-PSSnapin "Microsoft.sharepoint.powershell";
}

$web = Get-SPWeb $WebUrl

if($web -ne $null)
{
    $masterPageCatalogFolder = $web.GetFolder("_catalogs/masterpage")
    $customMasterPage = $masterPageCatalogFolder.Files[$MasterPageName];
    if ($customMasterPage)
    {
        $customMasterPage.Delete();
    }
    Write-Output "Master Page deleted successfully: $MasterPageName";
}
else
{
    throw "The site does not exist: $web"
}
$web.Dispose();