﻿# ------------------------------------------------------------------
# ---------------IIS Remove Web Site--------------------------------
# ------------------------------------------------------------------
PARAM(
        [Parameter(Mandatory=$true)][string]$WebSiteName
    )

#$WebSiteName = "Claims Workflow AF"

Import-Module WebAdministration

(Get-Website) | %{
    $Site = $_
    if($Site.name.ToLower() -eq $WebSiteName.ToLower())
    {
        Remove-Website -Name $WebSiteName
		Write-Output "Website has been removed"
    }
}