﻿
# ------------------------------------------------------------------
# --------------- Set up Windows Schedule Task ---------------------
# ------------------------------------------------------------------
# ------ Will delete existing instance and re-create from xml ------
# ------------------------------------------------------------------
# ------------- Then will set user account and enable --------------
# ------------------------------------------------------------------
PARAM(
        [Parameter(Mandatory=$true)][string]$TaskName,
        [Parameter(Mandatory=$true)][string]$ImportTaskXmlPath,
        [Parameter(Mandatory=$true)][string]$ServiceAccountUserName,
        [Parameter(Mandatory=$true)][string]$ServiceAccountUserPassword
    )

#$TaskName = "GCMSSchedule"
#$ImportTaskXmlPath = "\\development01\devteam$\Andy Fooks\GCMS2019\GCMSSchedule.xml"
#$ServiceAccountUserName = "EMEA\s-dms-00d-spk2"
#$ServiceAccountUserPassword = "Du6kosrc"

$schedule = Get-ScheduledTask -TaskName $TaskName

# If the schdule exists, delete it.
if($schedule) {     
    $null = Disable-ScheduledTask -TaskName $TaskName
    $null = Unregister-ScheduledTask -TaskName $TaskName -Confirm:$false
}

# Now add the schedule from the path provided
$null = Register-ScheduledTask -xml (Get-Content $ImportTaskXmlPath | Out-String) -TaskName $TaskName -User $ServiceAccountUserName -Password $ServiceAccountUserPassword –Force

# Enable the schedule
Enable-ScheduledTask -TaskName $TaskName

# Enable the triggers
$schedule.Triggers | % {
    $trigger = $_
    $trigger.Enabled = $true
}