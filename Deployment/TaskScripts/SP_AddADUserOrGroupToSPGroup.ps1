# ------------------------------------------------------------------
# -------Add AD group/user to SP Group--------------------
# ------------------------------------------------------------------
param(  [Parameter(Mandatory=$true)]$SPSiteUrl,
        [Parameter(Mandatory=$true)]$SharePointGroupName,
        [Parameter(Mandatory=$true)]$ADGroupOrUserName,
        [ValidateSet("Exact", "StartsWith", "EndsWith", "Contains")][Parameter(Mandatory=$true)]$SharePointGroupNameComparision)

if ( (Get-PSSnapin -Name "Microsoft.sharepoint.powershell" -ErrorAction SilentlyContinue) -eq $null )
{
    Add-PSSnapin "Microsoft.sharepoint.powershell";
}

#$SPSiteUrl = "http://dmsuktest.emea.ajgco.com/claims/dropbox"
#$SharePointGroupName = "readers"
#$ADGroupOrUserName = "EMEA\g-cwf-members-integration"
#$SharePointGroupNameComparision = "EndsWith"

$site = Get-SPSite -Identity $SPSiteUrl -ErrorAction SilentlyContinue
if(-not $site)
{
    throw "The Site could not be found"
}

function IsMatch($Matchtype, $SPGroupName, $targetGrpNameString) 
{
    if ($Matchtype -eq "Exact")
    {
        return ($SPGroupName.ToLower() -eq $targetGrpNameString.ToLower())
    }
    elseif ($Matchtype -eq "StartsWith")
    {
        return ($SPGroupName.ToLower().StartsWith($targetGrpNameString.ToLower()))
    }
    elseif ($Matchtype -eq "EndsWith")
    {
        return ($SPGroupName.ToLower().EndsWith($targetGrpNameString.ToLower()))
    }
    elseif ($Matchtype -eq "Contains")
    {
        return ($SPGroupName.ToLower().Contains($targetGrpNameString.ToLower()))
    }
    else
    {
      return $false
    }
}

$site.RootWeb.SiteGroups | %{

    $openGroup = $_ 
    
    if((IsMatch -Matchtype $SharePointGroupNameComparision -SPGroupName $openGroup.Name -targetGrpNameString $SharePointGroupName))
    {
			$user = $site.RootWeb.EnsureUser($ADGroupOrUserName)
			if ($user)
			{
				$openGroup.AddUser($user)
                $openGroup.Update()
				Write-Output ("Added user: " + $user.Name + ", To: " + $openGroup.Name)
			}
			else
			{
				Write-Output ("User: " + $currentUser.Name + " could not be found, so was not added to the security group.  Continuing anyway...")
			}
    }
}
$site.RootWeb.Update()