# ------------------------------------------------------------------
# -------Bulk Enable SP Feature for managed path-------------------
# ------------------------------------------------------------------
param(  [Parameter(Mandatory=$true)]$WebAppUrl,
        [Parameter(Mandatory=$true)]$FeatureID,
        [ValidateSet("Web", "Site")][Parameter(Mandatory=$true)]$Scope,
        [Parameter(Mandatory=$true)]$ManagedPath)

if ( (Get-PSSnapin -Name "Microsoft.sharepoint.powershell" -ErrorAction SilentlyContinue) -eq $null )
{
    Add-PSSnapin "Microsoft.sharepoint.powershell";
}

#$WebAppUrl = "http://dmsukdev.emea.ajgco.com"
#$Scope = "Site"
#$FeatureID = "3bae86a2-776d-499d-9db8-fa4cdc7884f8"
#$ManagedPath = "Claims"

$webApp = Get-SPWebApplication -Identity $WebAppUrl -ErrorAction SilentlyContinue
if(-not $webApp)
{
    throw "The Web Application could not be found"
}
$managedpath = Get-SPManagedPath -WebApplication $WebAppUrl -Identity $ManagedPath -ErrorAction SilentlyContinue
if(-not $managedpath)
{
    throw "The managed Path could not be found"
}

$webApp.Sites | Where-Object { $_.ServerRelativeUrl.ToLower().StartsWith("/" + $managedpath.Name.ToLower()) } | %{
    $Site = $_
    if($Scope.ToLower() -eq "web")
    {
        $Site.AllWebs | % {
            $SPWeb = $_
            if ($SPWeb.Features[$FeatureID] -eq $null)
            {
                Enable-SPFeature -Identity $FeatureID -url $SPWeb.Url -Confirm:$false -Force 
                Write-Output ("Web Feature Enabled for: " + $SPWeb.Url)
            }
            else
            {
                Write-Output ("Web Feature already Enabled for: " + $SPWeb.Url)
            }
        }
    }
    else
    {
        if ($Site.Features[$FeatureID] -eq $null)
        {
            Enable-SPFeature -Identity $FeatureID -url $Site.Url -Confirm:$false -Force 
            Write-Output ("Site Feature Enabled for site: " + $Site.Url)
        }
        else
        {
            Write-Output ("Site Feature already Enabled for: " + $Site.Url)
        }
    }
}