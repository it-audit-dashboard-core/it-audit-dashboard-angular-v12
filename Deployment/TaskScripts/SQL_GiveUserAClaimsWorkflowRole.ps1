# ------------------------------------------------------------------
# -------Give User Role on Claims Workflow User Role Table----------
# ------------------------------------------------------------------
param(  [Parameter(Mandatory=$true)][string]$ConnectionString,
        [Parameter(Mandatory=$true)][string]$UserName,
        [Parameter(Mandatory=$true)][int]$RoleId
     )

#$ConnectionString = "Data Source=SMIUKVDBD121;Initial Catalog=ClaimsWorkflow;Integrated Security=True;Pooling=False;MultipleActiveResultSets=true;"
#$UserName = "emea\DELETEME"
#$RoleId = "2"

# Create and open a database connection
$sqlConnection = new-object System.Data.SqlClient.SqlConnection $ConnectionString
$sqlConnection.Open()

# Create a command object
$sqlCommand = $sqlConnection.CreateCommand()
$sqlCommand.CommandText = "select * from [ClaimsWorkflow].[dbo].[User] where [Username] = '$UserName'"

$sqlReader = $sqlCommand.ExecuteReader()

$userExists = $false
[int]$userId = -1

# Get the User ID from the Users DB
if($sqlReader)
{
    while ($sqlReader.Read())
    {
        $username = $sqlReader["Username"]
        
        if($username.ToLower() -eq $UserName)
        {
            $userExists = $true
            $userId = $sqlReader["Id"]
            break;
        }
    }
}

if($userExists)
{
    # Create a command object
    $sqlCommand = $sqlConnection.CreateCommand()
    $sqlCommand.CommandText = "select * from [ClaimsWorkflow].[dbo].[UserRole] where [UserId] = $userId"

    $userHasRole = $false

    $sqlReader = $sqlCommand.ExecuteReader()
    
    # Check if the user already has the specified role
    if($sqlReader)
    {
        while ($sqlReader.Read())
        {
            $RoleFromSQL = $sqlReader["RoleId"]
            
            if($RoleFromSQL -eq $RoleId)
            {
                $userHasRole = $true
                break;
            }
        }
    }
    
    # Add the role for the user.
    if(-not $userHasRole)
    {   
        # Add the Role for the user
        $sqlCommand = $sqlConnection.CreateCommand()
        $sqlCommand.CommandText = "Insert into [ClaimsWorkflow].[dbo].[UserRole] (UserId, RoleId) Values ($userId, $RoleId)"
        $sqlReader = $sqlCommand.ExecuteReader()
        
        Write-Output "Giving User: $UserName, Role: $RoleId"
    } 
}
else
{
    throw "Specified user does not exist in the users Database"
}