# ------------------------------------------------------------------
# -------Set SP Regional Settings-----------------------------------
# ------------------------------------------------------------------
param(  [Parameter(Mandatory=$true)][string]$Url,
        [Parameter(Mandatory=$true)][string]$LCID,
        [Parameter(Mandatory=$true)][string]$CultureName)

if ( (Get-PSSnapin -Name "Microsoft.sharepoint.powershell" -ErrorAction SilentlyContinue) -eq $null )
{
    Add-PSSnapin "Microsoft.sharepoint.powershell";
}

$web = Get-SPWeb $Url

#Confirm Site Regional Settings are UK - Name(en-GB) - LCID(2057)
if($web.Locale.LCID -ne $LCID)
{
    $culture=[System.Globalization.CultureInfo]::CreateSpecificCulture($CultureName);
    $web.Locale=$culture;
    $web.Update(); 
    Write-Output (“Changed website regional settings to: " + $web.Locale.DisplayName + " successfully")
}
$web.Dispose();