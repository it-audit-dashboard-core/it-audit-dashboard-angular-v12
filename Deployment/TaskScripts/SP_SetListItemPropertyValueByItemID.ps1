# ------------------------------------------------------------------
# Set List itme attribute value - creates item if doesn't exist.
# ------------------------------------------------------------------
param(
        [Parameter(Mandatory=$true)][string]$SPWebUrl,
        [Parameter(Mandatory=$true)][string]$ListName,
        [Parameter(Mandatory=$true)][string]$ListItemID,
        [Parameter(Mandatory=$true)][string]$AttributeName,
        [Parameter(Mandatory=$true)][string]$AttributeValue
)

if ( (Get-PSSnapin -Name "Microsoft.sharepoint.powershell" -ErrorAction SilentlyContinue) -eq $null )
{
    Add-PSSnapin "Microsoft.sharepoint.powershell";
}

#$SPWebUrl = "http://dmsukdev.emea.ajgco.com/claims/DropBox"
#$ListName = "Site Configuration"
#$ListItemID = "4"
#$AttributeName = "cwfsiteconfigvalue" 
#$AttributeValue = "Something here"
#$AttributeName = "Title" 
#$AttributeValue = "SomeTitle"

$Web = Get-SPweb $SPWebUrl -ErrorAction SilentlyContinue
if(-not $Web)
{
    throw "The specified web could not be found"
}

$list = $Web.Lists[$ListName] #-ErrorAction SilentlyContinue
if(-not $list)
{
    throw "The specified List could not be found"
}

$item = $null

try
{
    $item = $list.Items.GetItemById($ListItemID)
}
catch{}

if(-not $item)
{
    $item = $list.Items.Add()
    if($ListItemID)
    {
        $list.Fields["ID"].ReadOnlyField = $false;
        $item[[Microsoft.SharePoint.SPBuiltInFieldId]::ID] = [convert]::ToInt32($ListItemID, 10)
        $list.Fields["ID"].ReadOnlyField = $true;
    }
}


if($AttributeName.ToLower() -ne "id")
{
    $item[$AttributeName] = $AttributeValue
}

$item.Update()