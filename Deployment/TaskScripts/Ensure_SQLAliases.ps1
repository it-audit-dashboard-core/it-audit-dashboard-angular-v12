# ------------------------------------------------------------------
# Create SQL Aliases
# ------------------------------------------------------------------
param(
	[Parameter(Mandatory=$true)][string]$xmlFilePath = $(throw "xmlFilePath is a required parameter."),
    [Parameter(Mandatory=$true)][ValidateSet("Dev", "Integration", "UAT", "Production")]$TargetSystem
)
#$xmlFilePath = "E:\AndyFooksTFS\ClaimsWorkflow\Dev\ClaimsWorkflow\AJG.Claims.SharePoint\Deployment\ClaimsSitesConfig.xml"
#$TargetSystem = "Dev"

# Check Xml File exists.
if(-not (Test-Path $xmlFilePath)){
    Write-Output "XmlFile Could not be found."
    throw
}
[xml]$configXmlFile = Get-Content $xmlFilePath

# Get Config Section
$EnvironmentConfigSection = $configXmlFile.SelectSingleNode("/Root/Environments/Environment[@target='" + $TargetSystem + "']")

# Now loop through the sitedetails objects and create stuff.
$EnvironmentConfigSection.SiteDetails | %{
    $SiteDetails = $_
    
    $AliasName = $SiteDetails.contentDatabaseAlias
    $AliasDataConnectionString = $SiteDetails.contentDatabaseAliasDBConnectionDetails
    $writable = $true
    ## The Aliase is stored in the registry here i'm seting the
    ## REG path for the Alias 32 & 64
    $Bit32 = "HKLM:\SOFTWARE\Microsoft\MSSQLServer\Client\ConnectTo"
    $Bit64 = "HKLM:\SOFTWARE\Wow6432Node\Microsoft\MSSQLServer\Client\ConnectTo" 
    
    ## chect if the ConnectTo SubFolder exists in the registry if not it will create it.
    If((Test-Path -Path $Bit32) -ne $true)    
    {       
        $key = (get-item HKLM:\).OpenSubKey("SOFTWARE\Microsoft\MSSQLServer\Client", $writable).CreateSubKey("ConnectTo")    
    }    
    If((Test-Path -Path $Bit64) -ne $true)    
    {        
        $key = (get-item HKLM:\).OpenSubKey("SOFTWARE\Wow6432Node\Microsoft\MSSQLServer\Client", $writable).CreateSubKey("ConnectTo")     
    }

    # If the Alias key doesn't exist, create it.
    if(-not (Get-ItemProperty -Path ($Bit32) -Name $AliasName -erroraction:silentlycontinue))
    {
        $null = New-ItemProperty -Path $Bit32 -Name $AliasName -PropertyType string -Value $AliasDataConnectionString
        Write-Output "Created 32 bit AliasName: $Alias Name, Alaise Value: $AliasDataConnectionString"
    }
    if(-not (Get-ItemProperty -Path ($Bit64) -Name $AliasName -erroraction:silentlycontinue))
    {
        $null = New-ItemProperty -Path $Bit64 -Name $AliasName -PropertyType string -Value $AliasDataConnectionString 
        Write-Output "Created 64 bit AliasName: $AliasName, Alaise Value: $AliasDataConnectionString"
    }
}