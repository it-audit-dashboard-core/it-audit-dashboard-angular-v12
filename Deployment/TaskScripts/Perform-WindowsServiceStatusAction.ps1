﻿# ------------------------------------------------------------------
# ---------------Perform a Windows Service Action-------------------
# ------------------------------------------------------------------
PARAM(
        [Parameter(Mandatory=$true)][string]$ServiceName,
        [ValidateSet("Start", "Stop", "Restart", "Pause", "Resume")][string]$ServiceAction
    )

function Set-WindowsServiceStartUpMode
{
    PARAM(
        [Parameter(Mandatory=$true)][string]$ServiceName,
        [ValidateSet("manual", "auto", "disabled")][string]$ServiceStartUpType
    )
    if($ServiceStartUpType.ToLower() -eq "auto")
    {
        Set-Service $ServiceName -startuptype "Automatic"
    }
    else
    {
        Set-Service $ServiceName -startuptype $ServiceStartUpType
    }
}
function Wait-ForServiceStatusToMatchExpectation
{
    PARAM(
        [Parameter(Mandatory=$true)][string]$ServiceName,
        [ValidateSet("stopped", "paused", "running", "starting", "stopping")][string]$ExpectedStatus,
        [Parameter(Mandatory=$true)][int]$Iterations,
        [Parameter(Mandatory=$true)][int]$TimeDelaySeconds
    )
    while($counter -lt $Iterations)
    {
        $service = Get-Service $ServiceName
        if($service.Status.ToString().ToLower() -ne $ExpectedStatus.ToString())
        {
            # wait before iterating again
            Start-Sleep -s $TimeDelaySeconds 
            $counter++
        }
        else
        {
            return
        }
    }
    throw ("The $ServiceName Service is not getting to the status of: $ExpectedStatus")
}
function Check-WindowsServiceStateMatchesExpectation
{
    PARAM(
        [Parameter(Mandatory=$true)][string]$ServiceName,
        [ValidateSet("stopped", "paused", "running", "starting", "stopping")][string]$ExpectedStatus
    )
    $service = Get-Service $ServiceName
    if($service.Status.ToString().ToLower() -eq $ExpectedStatus.ToLower())
    {
        return $True
    }
    else
    {
        return $False
    }
}
function Perform-WindowsServiceStatusAction
{
    PARAM(
        [Parameter(Mandatory=$true)][string]$ServiceName,
        [ValidateSet("Start", "Stop", "Restart", "Pause", "Resume")][string]$ServiceAction
    )
    
    $ServiceAction = $ServiceAction.ToLower()
    
    # Get the service
    $service = Get-Service $ServiceName

    # Get Service info including startup mode and stats
    $ServiceFromWMI = Get-WmiObject -Query "Select StartMode From Win32_Service Where Name='$ServiceName'"
    $ServiceStartUpMode = $ServiceFromWMI.StartMode.ToString()
    
    $serviceStatus = $service.Status.ToString().ToLower()
    $serviceStatus
    
    if(($serviceStatus -eq "starting") -or ($serviceStatus -eq "stopping"))
    {
        throw("Could not perform the requested action. The service is currently: $serviceStatus")
    }

    if($ServiceAction -eq "start")
    {
        if($serviceStatus -eq "stopped")
        {
            Write-Output ("The $ServiceName was in a stopped State: Strarting Service");
            Set-WindowsServiceStartUpMode -ServiceName $ServiceName -ServiceStartUpType "manual"
            Start-Service $ServiceName
            Set-WindowsServiceStartUpMode -ServiceName $ServiceName -ServiceStartUpType $ServiceStartUpMode
        }
        elseif($serviceStatus -eq "paused")
        {
            Write-Output ("The $ServiceName was in a Paused State: Strarting Service");
            Set-WindowsServiceStartUpMode -ServiceName $ServiceName -ServiceStartUpType "manual"
            Resume-Service $ServiceName
            Set-WindowsServiceStartUpMode -ServiceName $ServiceName -ServiceStartUpType $ServiceStartUpMode
        }
        Wait-ForServiceStatusToMatchExpectation -ServiceName $ServiceName -ExpectedStatus "running" -Iterations 10 -TimeDelaySeconds 6
        Write-Output ("The $ServiceName has been successfully started");
    }
    elseif($ServiceAction -eq "stop")
    {
        if($serviceStatus -eq "running")
        {
            # if the service is running, then check if it can be stoped and then stop it.
            if($service.CanStop)
            {
                Write-Output ("The $ServiceName was in a running State: Stopping Service");
                Set-WindowsServiceStartUpMode -ServiceName $ServiceName -ServiceStartUpType "manual"
                Stop-Service $ServiceName
                Set-WindowsServiceStartUpMode -ServiceName $ServiceName -ServiceStartUpType $ServiceStartUpMode
            }
            else
            {
                throw ("The $ServiceName service Cannot be stopped")
            }
        }
        elseif($serviceStatus -eq "paused")
        {
            Write-Output ("The $ServiceName was in a paused State: stopping Service");
            Set-WindowsServiceStartUpMode -ServiceName $ServiceName -ServiceStartUpType "manual"
            Stop-Service $ServiceName
            Set-WindowsServiceStartUpMode -ServiceName $ServiceName -ServiceStartUpType $ServiceStartUpMode
        }
        Wait-ForServiceStatusToMatchExpectation -ServiceName $ServiceName -ExpectedStatus "stopped" -Iterations 10 -TimeDelaySeconds 6
        Write-Output ("The $ServiceName has been successfully stopped");
    }
    elseif($ServiceAction -eq "pause")
    {
        if($service.CanPauseAndContinue)
        {
            if($serviceStatus -eq "running")
            {
                # If the service is running, we can pause it
                Write-Output ("The $ServiceName was in a running State: pausing Service");
                Set-WindowsServiceStartUpMode -ServiceName $ServiceName -ServiceStartUpType "manual"
                Suspend-Service $ServiceName
                Set-WindowsServiceStartUpMode -ServiceName $ServiceName -ServiceStartUpType $ServiceStartUpMode
            }
            elseif($serviceStatus -eq "stopped")
            {
                # If the service is already pause
                throw ("The $ServiceName could not be paused because it is in a $serviceStatus state")
            }
            Wait-ForServiceStatusToMatchExpectation -ServiceName $ServiceName -ExpectedStatus "paused" -Iterations 10 -TimeDelaySeconds 6
            Write-Output ("The $ServiceName has been successfully paused");
        }
        else
        {
            throw ("The $ServiceName service Cannot pause and continue")
        }
    }
    elseif($ServiceAction -eq "resume")
    {
        if($service.CanPauseAndContinue)
        {
            Write-Output ("The $ServiceName service will be resumed");
            Set-WindowsServiceStartUpMode -ServiceName $ServiceName -ServiceStartUpType "manual"
            Resume-Service $ServiceName
            Set-WindowsServiceStartUpMode -ServiceName $ServiceName -ServiceStartUpType $ServiceStartUpMode
        }
        else
        {
            throw ("The $ServiceName service Cannot be resumed")
        }
        Wait-ForServiceStatusToMatchExpectation -ServiceName $ServiceName -ExpectedStatus "running" -Iterations 10 -TimeDelaySeconds 6
        Write-Output ("The $ServiceName has been successfully resumed");
    }
    elseif($ServiceAction -eq "restart")
    {
        Write-Output ("The $ServiceName is presently in a $serviceStatus state.");
        Restart-Service $ServiceName
        Wait-ForServiceStatusToMatchExpectation -ServiceName $ServiceName -ExpectedStatus "running" -Iterations 10 -TimeDelaySeconds 6
        Write-Output ("The $ServiceName has been successfully restarted");
    }
}

Perform-WindowsServiceStatusAction -ServiceName $ServiceName -ServiceAction $ServiceAction