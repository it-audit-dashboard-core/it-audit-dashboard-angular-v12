﻿# ------------------------------------------------------------------
# ---------------Copy Files-----------------------------------------
# ------------------------------------------------------------------
PARAM(
        [Parameter(Mandatory=$true)][string]$SourcePath,
        [Parameter(Mandatory=$true)][string]$DestinationPath,
        [ValidateSet("true", "false")][string]$Recurse,
        [Parameter(Mandatory=$false)][string]$Include = ""
    )

#[string]$SourcePath = '\\emea\emeadata\A&S_Signings\Test\CATS_EWS\Custom EXE Solutions\*\Dev'
#[string]$DestinationPath = "\\emea\emeadata\PVS_UserData\afooks\My Documents\CATS EWS\New folder"
#[string]$Include = '*.config'
#[string]$Recurse = "true"

$recurseBool = [System.Convert]::ToBoolean($Recurse);

New-Item -Path $DestinationPath -ItemType "directory" -ErrorAction SilentlyContinue

if($recurseBool)
{
    $DirectoryItems = Get-ChildItem -Path $SourcePath -Recurse -Include $Include
}
else
{
    $DirectoryItems = Get-ChildItem -Path $SourcePath -Include $Include
}

function Do-Work{
    $DirectoryItems | % {
        $item = $_
        Copy-Item -Path $item.FullName -Destination $DestinationPath -ErrorAction SilentlyContinue -Force
        Write-Host "Copying: $($item.FullName) to $($DestinationPath)"
    }
}

try
{
    Do-Work
    Write-Host "Copy files has completed"
}
catch
{
    Write-Warning "Copy Files: An Exception Occured: $($Error[0])"
    throw ("Copy Files: An Exception Occured: $($Error[0])")
}