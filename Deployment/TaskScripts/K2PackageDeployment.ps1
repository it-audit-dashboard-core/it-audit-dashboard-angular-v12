param([Parameter(Mandatory=$true)][string]$KspxPackageLocation,[Parameter(Mandatory=$true)] [string]$k2HostServer)

#Adds the PowerShell Deployment snapin.
if ( (Get-PSSnapin -Name SourceCode.Deployment.PowerShell -ErrorAction SilentlyContinue) -eq $null )
{
    add-pssnapin SourceCode.Deployment.PowerShell
}

#testing data C:\Projects\AJG.K2.Utilities.Deployment\AJG.K2.SmartObject.Management\PowerShell\tiago test 1 Package.kspx

$strError = ''

$k2HostServer = 'Host=' + $k2HostServer + ';Port=5555;Integrated=True;IsPrimaryLogin=True'

#We don't need to specify -ConfigFile 'xml config file location here' as by default it will pick up a config file from the same directory and name of the package
#Scripts the deployment of a package


Deploy-Package -FileName $KspxPackageLocation -ConnectionString $k2HostServer -ErrorAction SilentlyContinue -ErrorVariable strError
 
if ($strError.Count -gt 0) {

  throw  $strError

}

<#
Available values for -ErrorAction :

https://msdn.microsoft.com/en-us/library/system.management.automation.actionpreference(v=vs.85).aspx

-Deployment log file-
By default, the deployment log file is saved to the same location as the package (*.KSPX) file used for the given deployment. Should any deployment errors occur and require troubleshooting, these errors will be recorded within the log file.

#>