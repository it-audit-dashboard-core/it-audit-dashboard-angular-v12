# ------------------------------------------------------------------
# Install/update Sites, Site columns and content types from xml File
# ------------------------------------------------------------------
param(
        [Parameter(Mandatory=$true)][string]$xmlFilePath = $(throw "xmlFilePath is a required parameter."),
        [Parameter(Mandatory=$true)][string]$LogFilePath = $(throw "LogFilePath is a required parameter."),
        [Parameter(Mandatory=$true)][ValidateSet("Dev", "Integration", "UAT", "Production")]$TargetSystem
)

function Write-LocalLog ($logMessage)
{
    # Write to the
    Write-Output $logMessage
    # Write to the log file.
    Add-Content -LiteralPath $LogFilePath -Value ("  *** Logged Direct From Script: " + $logMessage)
}

Write-LocalLog -logMessage "Entered CreateUpdate_SPSitesFromtemplateXML.ps1"

if ( (Get-PSSnapin -Name "Microsoft.sharepoint.powershell" -ErrorAction SilentlyContinue) -eq $null )
{
    Add-PSSnapin "Microsoft.sharepoint.powershell";
}

#$xmlFilePath = "E:\ClaimsWorkflowDeployments\AJG_ClaimsWorkflow_DeploymentPackage_08Oct2015_10_15\AJG_ClaimsWorkflow_SharePointDeployment\ClaimsSitesConfig.xml"
#$TargetSystem = "Dev"
#$LogFilePath = "C:\AJGWorkflowLog\somethins1\something2\test.txt"

# Ensure the logging directory path exists - it should because this will be called by the installer tool!!
[system.io.directory]::CreateDirectory($LogFilePath.Remove($LogFilePath.LastIndexOf('\')))

# Check Xml File exists.
if(-not (Test-Path $xmlFilePath)){
    Write-LocalLog -logMessage ("XmlFile Could not be found.")
    throw
}
[xml]$configXmlFile = Get-Content $xmlFilePath

# Get Config Section
$EnvironmentConfigSection = $configXmlFile.SelectSingleNode("/Root/Environments/Environment[@target='" + $TargetSystem + "']")
$TargetWebAppName = $EnvironmentConfigSection.webAppUrl
$TargetWebApp = Get-SPWebApplication -Identity $TargetWebAppName -ErrorAction:SilentlyContinue

# And Check that the farm is valid.
if(-not ($TargetWebApp)){
    Write-LocalLog -logMessage ("The specified web applciation" + $TargetWebAppName + "does not exist.")
    throw
}

# Now load the site configuration - This will be used for all site creations.
$SiteConfiguration = $configXmlFile.SelectSingleNode("/Root/ClaimsSiteConfiguration")

# Function to replace place holders e.g. %%atributename%% with value from Environment/sitedetails attribute value.
Function Replace-Placeholders($siteDetailsSection, $string)
{
    $matches = [regex]::matches($string, "(%{2}[\w\d]*%{2})")
    $matches | %{
        $match = $_
        #get matching attribute from site details node.
        $attributeValue = $siteDetailsSection.Attributes| Where-Object { $_.Name.ToLower() -eq $match.Value.ToLower().Replace("%%", "") }
        $string = $string.Replace($match.Value, $attributeValue.Value)
    }
    return $string
}

# Now loop through the sitedetails objects and create stuff.
$EnvironmentConfigSection.SiteDetails | %{
    $SiteDetails = $_
    
    $SiteServerRelativePath = $SiteDetails.urlPath + "/" + $SiteDetails.name
    $SiteUrl = $TargetWebAppName + $SiteServerRelativePath
    $Site = $TargetWebApp.Sites | Where-Object { $_.ServerRelativeUrl -eq $SiteServerRelativePath }

    Write-LocalLog -logMessage ("Working on Site: " + $TargetWebAppName + $SiteServerRelativePath)
    
    if(-not $Site)
    {
        # Ensure the managed path exists.
        $managedPath = Get-SpManagedPath -WebApplication $TargetWebAppName -Identity $SiteDetails.urlPath -ErrorAction SilentlyContinue
        
        if(-not $managedPath)
        {
            if($SiteDetails.urlPath)
    		{
                $managedPath = New-SPManagedPath -RelativeURL $SiteDetails.urlPath -WebApplication $TargetWebApp -ErrorAction SilentlyContinue
    		    Write-LocalLog -logMessage ("Created wildcard managed Path:" + $managedPath.Name + " PrefixType:" + $managedPath.PrefixType)
            }
    	}
        
        # Create content DB for site.
        $openContentDatabase = $null
		if ($SiteDetails.contentDatabaseAlias)
		{
			# Create a site collection specific content database.
			$contentDatabaseName = "WSS_Content_Claims_" + $SiteDetails.name
			$openContentDatabase = Get-SPContentDatabase -Identity $contentDatabaseName -ErrorAction SilentlyContinue
            
            if(-not ($openContentDatabase))
            {
    			Write-LocalLog -logMessage ("Creating content database Name: " + $contentDatabaseName + "Server: " + $SiteDetails.contentDatabaseAlias)
    			
    			$err = $null
    			$openContentDatabase = New-SPContentDatabase -DatabaseServer $SiteDetails.contentDatabaseAlias -Name $contentDatabaseName -WebApplication $TargetWebAppName -ErrorVariable err
    			
    			if ($err)
    			{
    				Write-LocalLog -logMessage ("Stopping because of a content database error.")
    				Break
    			}	
            }
		}
        
        # Create the site
        Write-LocalLog -logMessage ("Creating Site")
        $Site = New-SPSite -Url $SiteUrl -Template $SiteConfiguration.sitetemplate -OwnerAlias $SiteDetails.siteOwnerAlias -Name $SiteDetails.displayName -Description $SiteDetails.description -ContentDatabase $openContentDatabase -ErrorVariable err
    }
    
    # So, we should now definately have a site by this point
    $Web = $Site.RootWeb
    $culture = [System.Globalization.CultureInfo]::CreateSpecificCulture("en-GB")
	$Web.Locale = $culture
	$Web.Update()
    Write-LocalLog -logMessage ("Locale set: " + $Web.Locale)
    
    # Enable Sharepoint Site features
    if($SiteConfiguration.SiteFeatures.Feature)
    {
        $SiteConfiguration.SiteFeatures.Feature | % {
            $FeatureID = $_.id
            if ($Site.Features[$FeatureID] -eq $null)
            {
                Enable-SPFeature -Identity $FeatureID -url $Site.Url -Confirm:$false -Force
                Write-LocalLog -logMessage ("Site Feature Enabled: $FeatureID")
            }
            else
            {
                Write-LocalLog -logMessage ("Site Feature already Enabled: $FeatureID")
            }
        }
    }
    # Enable Sharepoint Web Features
    if($SiteConfiguration.WebFeatures.Features)
    {
        $SiteConfiguration.WebFeatures.Features | % {
            $FeatureID = $_.id
            if ($Web.Features[$FeatureID] -eq $null)
            {
                Enable-SPFeature -Identity $FeatureID -url $Web.Url -Confirm:$false -Force
                Write-LocalLog -logMessage ("Web Feature Enabled: $FeatureID")
            }
            else
            {
                Write-LocalLog -logMessage ("Web Feature already Enabled: $FeatureID")
            }
        }
    }
    
    # Set up Custom Role definitions
    $customRoleDefinitions = $SiteConfiguration.CustomRoleDefinitions
    if($customRoleDefinitions -and $customRoleDefinitions.RoleDefinition)
    {
        $customRoleDefinitions.RoleDefinition | %{
            $customRoleDefinition = $_
            
            $spRoleDefinition = $Web.RoleDefinitions[$customRoleDefinition.RoleDefinitionName];
            if($spRoleDefinition -eq $null){    
                # Role Definition Doesn't Exist
                Write-LocalLog -logMessage ("Adding Role Definition")
                $spRoleDefinition = New-Object Microsoft.SharePoint.SPRoleDefinition;
                $spRoleDefinition.Name = $customRoleDefinition.RoleDefinitionName;  
                $spRoleDefinition.Description = $customRoleDefinition.RoleDefinitionDescription;
                $spRoleDefinition.BasePermissions = $customRoleDefinition.BasePermissions_CommaSeperated    
                $Web.RoleDefinitions.Add($spRoleDefinition);
                Write-LocalLog -logMessage ("The Role definition has been added")
            }
            else
            {
                Write-LocalLog -logMessage ("The Role definition already exists, updating...")
                # The Role already exists...ensure the base permissions are correct.
                $spRoleDefinition.BasePermissions = $customRoleDefinition.BasePermissions_CommaSeperated
                $spRoleDefinition.Description = $customRoleDefinition.RoleDefinitionDescription;
                $spRoleDefinition.Update();
                Write-LocalLog -logMessage ("Upated the role definitions BasePermissions and Description")
            }
        }
    }
    
    # Set up site groups
    if($SiteConfiguration.Group)
    {
    	$SiteConfiguration.Group | % {
    		$currentGroup = $_
    		$groupName = $currentGroup.name
    		$groupName = Replace-Placeholders -siteDetailsSection $SiteDetails -string $groupName
    		$openGroup = $Web.SiteGroups[$groupName]
    		
            Write-LocalLog -logMessage ("Trying to create Group:" + $groupName)
            
            # Ensure the group exists
    		if ($openGroup -eq $null)
    		{
				Write-LocalLog -logMessage ("Site Owner: " + $Web.Site.Owner)
    			$Web.SiteGroups.Add($groupName, $Web.Site.Owner, $Web.Site.Owner, "Description")
    			$openGroup = $Web.SiteGroups[$groupName]
            }
            
            if($openGroup)
            {
                Write-LocalLog -logMessage ("The group has been retrieved: " + $groupName)
                
        		$roleAssignment = New-Object Microsoft.SharePoint.SPRoleAssignment($openGroup)
                
        		# Add the permission to group role.
        		$roleDefinition = $Web.RoleDefinitions[$currentGroup.Permission]
        		$roleAssignment.RoleDefinitionBindings.Add($roleDefinition)
        		$Web.RoleAssignments.Add($roleAssignment)
                
        		Write-LocalLog -logMessage ("Site group:" + $openGroup.Name + " Permission:" + $roleDefinition.Name)
        			
                if($currentGroup)
                {
                    Write-LocalLog -logMessage ("Current Group Found")
                }
                else
                {
                    Write-LocalLog -logMessage ("Current Group Not Found")
                }
                    		
                if($currentGroup.User)
                {
            		$currentGroup.User | % {
            			$currentUser = $_
            			if ($currentUser)
            			{
            				$user = $null
                            try{
            				    $user = $Web.Site.RootWeb.EnsureUser($currentUser.Name)
                            }
                            catch { Write-LocalLog -logMessage ("User: " + $currentUser.Name + " could not be found, so was not added to the security group.  Continuing anyway...") }
            				if ($user)
            				{
            					$openGroup.AddUser($user)
            					Write-LocalLog -logMessage ("Added user: " + $user.Name)
            				}
            				else
            				{
            					Write-LocalLog -logMessage ("User: " + $currentUser.Name + " could not be found, so was not added to the security group.  Continuing anyway...")
            				}
            			}
            		}
                }
            }
            else
            {
                Write-LocalLog -logMessage ("*-*-*-*-*-The group was not created!!!: " + $groupName)
            }
    	}
    }
    $Web.Update()
    
    # Add content types
    if($SiteConfiguration.ContentTypes.ContentType)
    {
        $SiteConfiguration.ContentTypes.ContentType | %{
            $xmlCT = $_
            $xmlCT.Name
            
            $contentTypeAdded = $false
            $contentTypeUpdated = $false
            
            # Loop through the fields and add if don't exist
            $xmlCT.Fields.Field | % {
                $ctField = $_
                #if($ctField.Group -eq "AJG Claims Workflow Columns")
                #{
                    if(-not $Web.AvailableFields.Contains($ctField.Id))
                    {
                       $Web.Fields.AddFieldAsXml($ctField.OuterXml)
                    }
                #}
            }
            $spContentType = $null
            # Add the CT type if it doesnt exist.
            if(-not $Web.AvailableContentTypes[$xmlCT.Name])
            {
                # Create the content type and add it to the site
                $spContentType = New-Object Microsoft.SharePoint.SPContentType ($xmlCT.ID,$Web.ContentTypes,$xmlCT.Name)
                $spContentType.Group = $xmlCT.Group
                $Web.ContentTypes.Add($spContentType)
                $contentTypeAdded = $true
            }
            
            # Now get the ct and ensure all the correct columns are added to it.
            $spContentType = $Web.ContentTypes[$xmlCT.Name]
            
            # Now add the fields to the Content type
            $xmlCT.Fields.Field | % {
                $ctField = $_
                $ctField.DisplayName
                #if($ctField.Group -eq "AJG Claims Workflow Columns")
                #{
                    $ctField.DisplayName
                    if(-not $spContentType.FieldLinks[$ctField.StaticName])
                    {
                        $ctField.DisplayName
                        $spfield = $Web.Fields[$ctField.DisplayName]
                        $link = new-object Microsoft.SharePoint.SPFieldLink $spfield
                        $spContentType.FieldLinks.Add($link)
                        $spContentType.Update($true)
                        $contentTypeUpdated = $true
                    }
                #}
            }
            
            # Update all list content types.
            if((-not $contentTypeAdded) -and ($contentTypeUpdated))
            {
                # Now update all the references to this content type.
                $spContentType = $Web.ContentTypes[$xmlCT.Name]
                $spContentType.Update($true)
                $usages = [Microsoft.Sharepoint.SPContentTypeUsage]::GetUsages($spContentType)
                $usages | % {
                    $usage = $_
                    if($usage.IsUrlToList)
                    {
                        # Get the library name
                        $listName = $usage.Url.Split('/')[$usage.Url.Split('/').Count-1]
                        $siteUrl = $Web.Site.WebApplication.Url.TrimEnd('/') + $usage.Url.Remove($usage.Url.LastIndexOf('/'), $listName.Length+1)
                        $usageWeb = Get-SPWeb $siteUrl
                        $usageList = $usageWeb.Lists[$listName]
                        $usageCT = $usageList.ContentTypes[$xmlCT.Name]
                        $usageCT.Update()
                        $usageList.Update()
                        $usageWeb.Dispose()
                    }
                }
            }

            $spContentType.Update($true)
        }
    }
    
    # Set up lists - ignore lookup lists, they are not supported in this version of this script.
    if($SiteConfiguration.List)
    {
    	$SiteConfiguration.List | ForEach-Object {
    	    $currentList = $_
    		$listName = $currentList.name
    		$listName = Replace-Placeholders -siteConfig $SiteConfiguration -string $listName
            $openList = $null
            $openList = $Web.Lists[$listName]
            Write-LocalLog -logMessage ($listName)
            # Ensure list exists
            if(-not $openList)
            {
                $listTemplate = [Microsoft.SharePoint.SPListTemplateType]::GenericList
        		$openList = $Web.Lists.Add($listName, $currentList.description, $listTemplate)
        		$openList = $Web.Lists[$listName]
        		$openList.OnQuickLaunch = ($currentList.onQuickLaunch -ne "false")
        		$openList.Hidden = ($currentList.hidden -eq "true")
                $openList.Update()
                $Web.Update()
        		Write-LocalLog -logMessage ("List Created: " + $openList.Title)
            }
            
            # Add Content types
            if ($currentList.ContentType)
            {
                # Enable content types
                $openList.ContentTypesEnabled = $true
                
                # Remove the default Item Content type if not specified in list
                $ItemContentExistsInListConfig = $currentList.ContentType | Where-Object { $_.name -eq "Item" } 
                $itemContentType = $openList.ContentTypes["Item"]
                if($itemContentType -and (-not $ItemContentExistsInListConfig))
                {
				    $documentContentType = $openList.ContentTypes.Delete($itemContentType.Id)
                    Write-LocalLog -logMessage ("Removed Content Type: " + $itemContentType.Name)
                }
                
                $currentList.ContentType | % {
                    $currentContentType = $_
                    $contentType = $Web.Site.RootWeb.ContentTypes[$currentContentType.name]
    				if($contentType)
    				{
                        Write-LocalLog -logMessage ("Adding  Content Type: " + $contentType.Name)
                        if(-not $openList.ContentTypes[$contentType.Name])
                        {
    					   $contentType = $openList.ContentTypes.Add($contentType)
                        }
    				}
    				else
    				{
    					Write-LocalLog -logMessage ("Error, could not find site Content Type: " + $currentContentType.name)
    				}
                }
                $openList.Update()
            }
            
    		# Add list items
			if($currentList.Item.Count > 0)
            {
    			$currentList.Item | ForEach-Object {
    				$currentConfigListItem = $_
					$item = $null
					if($currentConfigListItem.ID)
					{
						try
						{
							$item = $openList.Items.GetItemById($currentConfigListItem.ID)
						}
						catch{}
					}
					if(-not $item)
					{
						$item = $openList.Items.Add()
						if($currentConfigListItem.ID)
						{
							$openList.Fields["ID"].ReadOnlyField = $false;
							$item[[Microsoft.SharePoint.SPBuiltInFieldId]::ID] = [convert]::ToInt32($currentConfigListItem.ID, 10)
							$openList.Fields["ID"].ReadOnlyField = $true;
						}
					}

					$currentConfigListItem.Attributes | %{                    
						$attr = $_
						if($attr.Name.ToLower() -ne "id")
						{
							$item[$attr.Name] = Replace-Placeholders -siteDetailsSection $SiteDetails -string $attr.Value 
						}
					}
					$item.Update()
    			}
			}
            $Web.Update()
    	}
    }
    # Create The libraries
    if($SiteConfiguration.DocumentLibrary)
    {
    	# Loop for each DocumentLibrary element under this SiteCollection in the xml configuration file.
    	$SiteConfiguration.DocumentLibrary | % {
    	    $currentLibrary = $_
    		
    		$libraryName = $currentLibrary.name
    		$libraryName = Replace-Placeholders -siteDetailsSection $SiteDetails -string $libraryName
    		
    		$openList = $null
    		$openList = $Web.Lists[$libraryName]
    		if (-not $openList)
    		{
                $libraryListTemplate = [Microsoft.SharePoint.SPListTemplateType]::DocumentLibrary
    			$openList = $Web.Lists.Add($libraryName, $currentLibrary.description, $libraryListTemplate)
    			$openList = $Web.Lists[$libraryName]
    			Write-LocalLog -logMessage ("Library Created: " + $openList.Title)
    		}
    		
            # We should now definately have a library
    		$openList.OnQuickLaunch = ($currentLibrary.onQuickLaunch -ne "false")
    		$openList.EnableVersioning = ($currentLibrary.enableVersioning -ne "false")
    		$openList.ForceCheckout = ($currentLibrary.requireCheckout -eq "true")
    		$openList.EnableFolderCreation = ($currentLibrary.enableFolderCreation -ne "false")
    		
    		if ($currentLibrary.majorVersionLimit)
    		{
    			$openList.EnableVersioning = $true
    			$openList.MajorVersionLimit = $currentLibrary.majorVersionLimit
    			Write-LocalLog -logMessage ("Set Major Version Limit: " + $openList.MajorVersionLimit)
    		}
    		
    		if ($currentLibrary.majorWithMinorVersionsLimit)
    		{
    			$openList.EnableVersioning = $true
    			$openList.EnableMinorVersions = $true
    			$openList.MajorWithMinorVersionsLimit = $currentLibrary.majorWithMinorVersionsLimit
    			Write-LocalLog -logMessage ("Set Major With Minor Versions Limit: " + $openList.MajorWithMinorVersionsLimit)
    		}

    		if ($currentLibrary.draftVersionVisibility -eq "true")
    		{
    			$openList.DraftVersionVisibility = $true
    		}

    		if ($currentLibrary.hidden -eq "true")
    		{
    			$openList.Hidden = $true
    		}
    		
    		if ($currentLibrary.itemLevelReadSecurityUsersCanSeeOnlyItemsTheyCreate -eq "true")
    		{
    			$openList.ReadSecurity = 2
    		}
    		if ($currentLibrary.itemLevelWriteSecurityUsersCanModifyOnlyItemsTheyCreate -eq "true")
    		{
    			$openList.WriteSecurity = 2
    		}
    		# Add Content types
            if ($currentLibrary.ContentType)
            {
                # Enable content types
                $openList.ContentTypesEnabled = $true
                
                # Remove the default Item Content type if not specified in list
                $DocumentContentExistsInListConfig = $currentLibrary.ContentType | Where-Object { $_.name -eq "Document" } 
                $DocumentContentType = $openList.ContentTypes["Document"]
                if($DocumentContentType -and (-not $DocumentContentExistsInListConfig))
                {
				    $documentContentType = $openList.ContentTypes.Delete($DocumentContentType.Id)
                    Write-LocalLog -logMessage ("Removed Content Type: " + $DocumentContentType.Name)
                }
                
                $currentLibrary.ContentType | % {
                    $currentContentType = $_
                    $contentType = $Web.Site.RootWeb.ContentTypes[$currentContentType.name]
    				if($contentType)
    				{
                        Write-LocalLog -logMessage ("Adding  Content Type: " + $contentType.Name)
                        if(-not $openList.ContentTypes[$contentType.Name])
                        {
    					   $contentType = $openList.ContentTypes.Add($contentType)
                        }
    				}
    				else
    				{
    					Write-LocalLog -logMessage ("Error, could not find site Content Type: " + $currentContentType.name)
    				}
                }
                $openList.Update()
            }
    		
    		$firstLibraryPermission = $true
    		$openList.Update()
    	}
    }
}