﻿# ------------------------------------------------------------------
# ---------------IIS Application Pool-------------------------------
# ------------------------------------------------------------------
PARAM(
        [ValidateSet("EnsureAppPool", "RemoveAppPool")][string]$Action,
        [Parameter(Mandatory=$true)][string]$AppPoolName,
        [Parameter(Mandatory=$false)][string]$AppPoolUserName,
        [Parameter(Mandatory=$true)][string]$AppPooluserPassword,
        [ValidateSet("v2.0", "v4.0")][string]$AppPoolManagedRuntimeVersion,
        [ValidateSet("Classic", "Integrated")][string]$AppPoolManagedPipelineMode
    )

#$Action = "EnsureAppPool"
#$AppPoolName = "ANDYTEST"
#$AppPoolUserName = "EMEA\s-dms-00t-spadmin"
#$AppPooluserPassword = "5GHGWd5oUF7R"
#$AppPoolManagedRuntimeVersion = "v4.0"
#$AppPoolManagedPipelineMode = "Integrated"

Import-Module WebAdministration

$AppPool = $null
try
{
    $AppPool = Get-WebAppPoolState -Name $AppPoolName -ErrorAction SilentlyContinue -WarningAction SilentlyContinue
}
catch {}

if($Action.ToLower() -eq "ensureapppool")
{
    if($AppPool)
    {
        Write-Output "The app pool already Exists, Setting properties...Recreating"
        
        # We want to remove the app pool
        Remove-WebAppPool -Name $AppPoolName -ErrorAction SilentlyContinue
        
        Write-Output "Removed the application pool"
    }
    
    Write-Output "The app pool is being created..."
    
    # The app pool doesnt exist, so create it with the specified settings.
    $AppPool = New-WebAppPool -Name $AppPoolName -Force
    $AppPool.processModel.userName = $AppPoolUserName
    $AppPool.processModel.password = $AppPooluserPassword
    $AppPool.processModel.identityType = 3
    $AppPool.managedRuntimeVersion = $AppPoolManagedRuntimeVersion
    $AppPool.managedPipelineMode = $AppPoolManagedPipelineMode

    $AppPool|Set-Item
    
    Write-Output "The app pool has been created"
}
elseif($Action.ToLower() -eq "removeapppool")
{
    if($AppPool)
    {
        # We want to remove the app pool
        Remove-WebAppPool -Name $AppPoolName -ErrorAction SilentlyContinue
    }
}