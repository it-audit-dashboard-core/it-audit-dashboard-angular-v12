﻿# ------------------------------------------------------------------
# ------Update ConfigSettings value with latest migration value-----
# ------------------------------------------------------------------
param(  
		[Parameter(Mandatory=$true)][string]$XMLDocPath,
        [Parameter(Mandatory=$true)][string]$AttributeName,
        [Parameter(Mandatory=$true)][string]$StartXPathSingleNode,
        [Parameter(Mandatory=$true)][string]$RecurseChildNodes,
        [Parameter(Mandatory=$false)][string]$NamespacePrefix,
        [Parameter(Mandatory=$false)][string]$MigrationsFolderFullPath
      )

#$XMLDocPath = "E:\AndyFooksTFS\ClaimsWorkflow\Dev\ClaimsWorkflow\AJG.Claims.PackageBuildAndDeployment\bin\Debug\AJG_ClaimsWorkflow_DeploymentPackage\AJG_ClaimsWorkflow_MVCSiteDeployment\ConfigSettings.xml" 
#$AttributeName = "Value" 
#$StartXPathSingleNode = "//ns:Settings/ns:Environments/ns:Environment/ns:Variables/ns:Variable[@Name='%%TargetMigration%%']" 
#$RecurseChildNodes = "False" 
#$NamespacePrefix = "ns"
#$MigrationsFolderFullPath = "E:\AndyFooksTFS\ClaimsWorkflow\Dev\ClaimsWorkflow\AJG.Claims.DAL\Migrations"

# Get the latest migration from downloaded migrations
$LatestMigrationName = "";
$LatestMigration = Get-ChildItem $MigrationsFolderFullPath -Filter *.resx -Recurse | sort -Descending | Select-Object -First 1
if($LatestMigration)
{
    # Now get the name of the migration
    $LatestMigrationName = $LatestMigration.Name.Split(@('_', '.'))[1]
}

# Now update the config Settings attribute value.
$RecurseChildNodesBool = [System.Convert]::ToBoolean($RecurseChildNodes)
function ParseXmlEmelment($xmlNode, $AttributeName, $AttributeValue, $RecurseChildNodes)
{
    if($xmlNode.HasAttribute($AttributeName))
    {
        $xmlNode.SetAttribute($AttributeName, $AttributeValue)
    }
    if($ResurseChildNodes)
    {
        $xmlNode.ChildNodes | %{
            $currentNode = $_
            ParseXmlEmelment -xmlNode $currentNode -attributeName $AttributeName -AttributeValue $AttributeValue -RecurseChildNodes $RecurseChildNodes
        }
    }
}

# Now set the xml attribute.
if(Test-Path $XMLDocPath)
{
    [xml]$document = Get-Content $XMLDocPath
    if($NamespacePrefix -eq "")
    {
        $XpathNode = $document.SelectNodes($StartXPathSingleNode)
    }
    else
    {
        $ns = New-Object System.Xml.XmlNamespaceManager($document.NameTable)
        $ns.AddNamespace($NamespacePrefix, $document.DocumentElement.NamespaceURI)
        $XpathNode = $document.SelectNodes($StartXPathSingleNode, $ns)
    }
    
    if($XpathNode)
    {
        $XpathNode | %{
            $Node = $_
            $Node.Attributes

            ParseXmlEmelment -xmlNode $Node -attributeName $AttributeName -AttributeValue $LatestMigrationName -RecurseChildNodes $RecurseChildNodesBool
        }
        $document.Save($XMLDocPath)
    }
    else
    {
        Write-Output "No Matching Output found."
    }
}
else
{
    throw ("The terms xml doc couldn't be found: " + $XMLDocPath)
}