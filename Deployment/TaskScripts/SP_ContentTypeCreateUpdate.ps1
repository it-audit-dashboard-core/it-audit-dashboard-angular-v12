﻿param(
	[string] $xmlFilePath = $(throw "xmlFilePath is a required parameter."),
	[string] $siteURL = $(throw "siteURL is a required parameter.")
)

if ( (Get-PSSnapin -Name "Microsoft.sharepoint.powershell" -ErrorAction SilentlyContinue) -eq $null )
{
    Add-PSSnapin "Microsoft.sharepoint.powershell";
}

#$xmlFilePath = "C:\AJG_ClaimsWorkflow_DeploymentPackage\contentTypes.xml"
#$siteURL = "http://dmsuktest.emea.ajgco.com/dms/claims"

$destSite = Get-SPSite $siteURL -ErrorAction SilentlyContinue
$destWeb = $destSite.RootWeb

if ($destWeb -eq $null)
{
	Write-Output "Error, site could not be opened."
	Break
}

if(-not(Test-Path -Path $xmlFilePath))
{
    Write-Output "Error, Xml File could not be found."
	Break
}

# Loop through the content types
[xml]$xmlDoc = Get-Content $xmlFilePath

$xmlDoc.ContentTypes.ContentType | %{
    $xmlCT = $_
    $xmlCT.Name
    
    $contentTypeAdded = $false
    $contentTypeUpdated = $false
    
    # Loop through the fields and add if don't exist
    $xmlCT.Fields.Field | % {
        $ctField = $_
        if($ctField.Group -eq "AJG Claims Workflow Columns")
        {
            if(-not $destWeb.AvailableFields.Contains($ctField.Id))
            {
               $destWeb.Fields.AddFieldAsXml($ctField.OuterXml)
            }
        }
    }
    $spContentType = $null
    # Add the CT type if it doesnt exist.
    if(-not $destWeb.AvailableContentTypes[$xmlCT.Name])
    {
        # Create the content type and add it to the site
        $spContentType = New-Object Microsoft.SharePoint.SPContentType ($xmlCT.ID,$destWeb.ContentTypes,$xmlCT.Name)
        $spContentType.Group = $xmlCT.Group
        $destWeb.ContentTypes.Add($spContentType)
        $contentTypeAdded = $true
    }
    
    # Now get the ct and ensure all the correct columns are added to it.
    $spContentType = $destWeb.ContentTypes[$xmlCT.Name]
    
    # Now add the fields to the Content type
    $xmlCT.Fields.Field | % {
        $ctField = $_
        $ctField.DisplayName
        if($ctField.Group -eq "AJG Claims Workflow Columns")
        {
            $ctField.DisplayName
            if(-not $spContentType.FieldLinks[$ctField.StaticName])
            {
                $ctField.DisplayName
                $spfield = $destWeb.Fields[$ctField.DisplayName]
                $link = new-object Microsoft.SharePoint.SPFieldLink $spfield
                $spContentType.FieldLinks.Add($link)
                $spContentType.Update($true)
                $contentTypeUpdated = $true
            }
        }
    }
    
    # Update all list content types.
    #if((-not $contentTypeAdded) -and ($contentTypeUpdated))
    #{
    #    # Now update all the references to this content type.
    #    $spContentType = $destWeb.ContentTypes[$xmlCT.Name]
    #    $spContentType.Update($true)
    #    $usages = [Microsoft.Sharepoint.SPContentTypeUsage]::GetUsages($spContentType)
    #    $usages | % {
    #        $usage = $_
    #        if($usage.IsUrlToList)
    #        {
    #            # Get the library name
    #            $listName = $usage.Url.Split('/')[$usage.Url.Split('/').Count-1]
    #            $siteUrl = $destSite.WebApplication.Url.TrimEnd('/') + $usage.Url.Remove($usage.Url.LastIndexOf('/'), $listName.Length+1)
    #            $usageWeb = Get-SPWeb $siteUrl
    #            $usageList = $usageWeb.Lists[$listName]
    #            $usageCT = $usageList.ContentTypes[$xmlCT.Name]
    #            $usageCT.Update()
    #            $usageList.Update()
    #            $usageWeb.Dispose()
    #        }
    #    }
    #}

    #$spContentType.Update($true)
}

$destSite.Dispose()
$destWeb.Dispose()