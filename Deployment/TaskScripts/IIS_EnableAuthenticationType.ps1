﻿# ------------------------------------------------------------------
# ---------------IIS enable authentication types--------------------
# ------------------------------------------------------------------
PARAM(
        [Parameter(Mandatory=$true)][string]$SiteName,
        [ValidateSet("windows", "anonymous", "digest", "basic")][string]$AuthenticationType,
        [ValidateSet("true", "false")][string]$Enable
    )

#$AuthenticationType = "basic"
#$SiteName = "Claims Workflow MVC Site"
#$Enable = "true"

$EnableBool = [System.Convert]::ToBoolean($Enable);

$PropertyFilter = "/system.WebServer/security/authentication/" + $AuthenticationType.ToLower() + "Authentication"

Import-Module WebAdministration

Set-WebConfigurationProperty -filter $PropertyFilter -name enabled -value $EnableBool -PSPath "IIS:\" -Location $SiteName