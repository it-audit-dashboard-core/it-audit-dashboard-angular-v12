# ------------------------------------------------------------------
# -----------------Purge Claim from Claims Database-----------------
# ------------------------------------------------------------------
param(  [Parameter(Mandatory=$true)][string]$ClaimFileId,
        [Parameter(Mandatory=$true)][string]$ConnectionString
       )

#$ClaimFileId = "FI0018812AA"
#$ConnectionString = "Data Source=SMIUKVDBD121;Initial Catalog=ClaimsWorkflow;Integrated Security=True;Pooling=False;MultipleActiveResultSets=true;"

# Create and open a database connection
$sqlConnection = new-object System.Data.SqlClient.SqlConnection $ConnectionString
$sqlConnection.Open()
 
#Create a command object
$sqlCommand = $sqlConnection.CreateCommand()
$sqlCommand.CommandText = "select Id from [ClaimsWorkflow].[dbo].[ClaimFile] where [ClaimRef] = '$ClaimFileId'"
 
#Execute the Command
$sqlReader = $sqlCommand.ExecuteReader()

# Now delete: The Diary Entries, ClaimFile row and SP doc set.
while ($sqlReader.Read()) 
{ 
    $ClaimId = $sqlReader["Id"]

    Write-Output "Deleting Diary Entries from [ClaimsWorkflow].[dbo].[Diary] for claim ref: $ClaimFileId, Claim Id: $ClaimId"
    # Delete All Diary Entries for this claim.
    $sqlCommand = $sqlConnection.CreateCommand()
    $sqlCommand.CommandText = "DELETE FROM [ClaimsWorkflow].[dbo].[Diary] WHERE ClaimFileId = '$ClaimId'"
    $sqlReader = $sqlCommand.ExecuteReader()
    Write-Output "Deleted Diary Entries from [ClaimsWorkflow].[dbo].[Diary] for claim ref: $ClaimFileId, Claim Id: $ClaimId"

    Write-Output "Deleting ClaimFile Entry from [ClaimsWorkflow].[dbo].[ClaimFile] for claim ref: $ClaimFileId, Claim Id: $ClaimId"
    # Delete the Claim from the Claims Table.
    $sqlCommand = $sqlConnection.CreateCommand()
    $sqlCommand.CommandText = "DELETE FROM [ClaimsWorkflow].[dbo].[ClaimFile] WHERE Id = '$ClaimId'"
    $sqlReader = $sqlCommand.ExecuteReader()
    Write-Output "Deleted ClaimFile Entry from [ClaimsWorkflow].[dbo].[ClaimFile] for claim ref: $ClaimFileId, Claim Id: $ClaimId"
}
 
# Close the database connection
$sqlConnection.Close()