﻿# ------------------------------------------------------------------
# ---------------Delete FileSystem Item-----------------------------
# ------------------------------------------------------------------
PARAM(
        [Parameter(Mandatory=$true)][string]$ItemPath,
        [ValidateSet("true", "false")][string]$Recurse
    )

#[string]$ItemPath = "C:\inetpub\Test4"
#[string]$Recurse = "true"

$recurseBool = [System.Convert]::ToBoolean($Recurse);

if(Test-Path $ItemPath)
{ 
    if($recurseBool)
    {
        Remove-Item $ItemPath -Recurse -Confirm:$False
    }
    else
    {
        Remove-Item $ItemPath -Confirm:$False
    }
    Write-Output "All items have been deleted"
}
else
{
    Write-Output "Path Does not exist"
}