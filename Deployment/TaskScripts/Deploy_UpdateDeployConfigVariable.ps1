# ------------------------------------------------------------------
# -------Update Deployment config Variable Value--------------------
# ------------------------------------------------------------------
param(  [Parameter(Mandatory=$true)][string]$FilePath,
        [Parameter(Mandatory=$true)][string]$DeploymentTargetEnum,
        [Parameter(Mandatory=$true)][string]$VariableName,
        [Parameter(Mandatory=$false)][string]$VariableValue
       )

#$FilePath = "C:\Users\afooks\source\repos\cats-ews\AJG.CATS.ExchangeWebService\AJG.CATS.ExchangeWebService\bin\Debug\AJG_CAST_EWS_DeploymentPackage\DeploymentConfig.xml"
#$DeploymentTargetEnum = "DEV"
#$VariableName = "%EncryptionKey%"
#$VariableValue = "Hello World"
$XPath = "//Settings/Environments/Environment[@DeploymentTargetEnum='$DeploymentTargetEnum']/Variables/Variable[@Name='$VariableName']"
$AttributeName = "Value"
function ParseXmlEmelment($xmlNode, $AttributeName, $AttributeValue)
{
    if($xmlNode.HasAttribute($AttributeName))
    {
        $xmlNode.SetAttribute($AttributeName, $AttributeValue)
    }
}
if(Test-Path $FilePath)
{
    [xml]$document = Get-Content $FilePath
    $XpathNode = $document.SelectSingleNode($XPath)    
    if($XpathNode)
    {
        $XpathNode | %{
            $Node = $_
            $Node.Attributes
            ParseXmlEmelment -xmlNode $Node -attributeName $AttributeName -AttributeValue $VariableValue
        }
        $document.Save($FilePath)
    }
    else
    {
        Write-Output "No Matching Output found."
    }
}
else
{
    throw ("The xml doc couldn't be found: " + $FilePath)
}