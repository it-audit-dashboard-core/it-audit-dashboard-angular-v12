# ------------------------------------------------------------------
# ------Update ConfigSettings value with latest migration value-----
# ------------------------------------------------------------------
param(
        [Parameter(Mandatory=$true)][string]$SharePointSiteUrl,
        [Parameter(Mandatory=$true)][string]$ListName,
        [Parameter(Mandatory=$true)][string]$Title,
        [Parameter(Mandatory=$false)][string]$DeploymentTarget,
        [Parameter(Mandatory=$false)][string]$TFSBuildNumber,
        [Parameter(Mandatory=$false)][string]$ServerName,
        [Parameter(Mandatory=$false)][string]$Comments
      )

# The uri refers to the path of the service description, e.g. the .asmx page            
#$SharePointSiteUrl = "http://sharepointuk.emea.ajgco.com/sites/ClaimsWorkflowProject"                     
#$ListName = "Deployment Tracker"
#$Title = "Title1"
#$DeploymentTarget = "dt"
#$TFSBuildNumber = "tfsbuildno"
#$ServerName = "server"
#$Comments = "comment"

$SharePointSiteUrl = $SharePointSiteUrl + "/_vti_bin/lists.asmx?WSDL"

$SPService = New-WebServiceProxy -uri $SharePointSiteUrl -NameSpace SpWs -UseDefaultCredential
$ListInfo = $SPservice.GetListandView($ListName,"")
$ListID  = $ListInfo.List.Name
$ViewID  = $ListInfo.View.Name
 
# build the XML 'batch' of entries that make up the Item
$doc = new-object "System.Xml.XmlDocument"
$batch = $doc.CreateElement("Batch")
$batch.SetAttribute("OnError", "Continue")
$batch.SetAttribute("ListVersion", "1")
$batch.SetAttribute("ViewName", $ViewID)

$batch.InnerXml = "<Method ID='1' Cmd='New'>" + 
                        "<Field Name='Title'>$Title </Field>" + 
                        "<Field Name='DeploymentTarget'>$DeploymentTarget</Field>" + 
                        "<Field Name='TFSBuildNumber'>$TFSBuildNumber</Field>" + 
                        "<Field Name='ServerName'>$ServerName</Field>" + 
                        "<Field Name='Comments'>$Comments</Field>" + 
                   "</Method>"
$response = $SPservice.UpdateListItems($ListID, $batch)