# ------------------------------------------------------------------
# -------Uninstall SP Feature---------------------------------------
# ------------------------------------------------------------------
param(  [Parameter(Mandatory=$true)]$Url, 
        [Parameter(Mandatory=$true)]$FeatureID, 
        [Parameter(Mandatory=$true)]$Scope)

if ( (Get-PSSnapin -Name "Microsoft.sharepoint.powershell" -ErrorAction SilentlyContinue) -eq $null )
{
    Add-PSSnapin "Microsoft.sharepoint.powershell";
}

if($Scope.ToLower() -eq "web")
{
    $SPEntity = Get-SPWeb $Url
}
if($Scope.ToLower() -eq "site")
{
    $SPEntity = Get-SPSite $Url
}

if($SPEntity -eq $null)
{
    throw "$Scope does not exist: $SPEntity"
}
else
{
    if ($SPEntity.Features[$FeatureID] -eq $null)
    {
        Uninstall-SPFeature -Identity $FeatureID -Confirm:$false -Force
        Write-Output "Feature Uninstalled: $FeatureID"
    }
    else
    {
        Write-Output "Feature already Enabled: $FeatureID"
        throw "This Feature is still Activated"
    }
}
$SPEntity.Dispose()