# ------------------------------------------------------------------
# -------Set Master Page--------------------------------------------
# ------------------------------------------------------------------
param(  [Parameter(Mandatory=$true)][string]$WebUrl,
        [Parameter(Mandatory=$true)][string]$CustomMasterUrl,
        [Parameter(Mandatory=$true)][string]$MasterUrl)

if ( (Get-PSSnapin -Name "Microsoft.sharepoint.powershell" -ErrorAction SilentlyContinue) -eq $null )
{
    Add-PSSnapin "Microsoft.sharepoint.powershell";
}

$web = Get-SPWeb $WebUrl

if($web -ne $null)
{
    $web.CustomMasterUrl = $CustomMasterUrl;
    $web.MasterUrl = $MasterUrl;
    $web.Update();
    Write-Output("Master Page changed successfully");
}
else
{
    throw "The site does not exist: $web"
}
$web.Dispose();