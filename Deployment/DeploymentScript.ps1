﻿# ------------------------------------------------------------------
# ---------------Deployment Script----------------------------------
# ------------------------------------------------------------------
param(  [Parameter(Mandatory=$true)][string]$deploymentTarget,
        [Parameter(Mandatory=$false)][string]$tasksFile = "DeploymentTasks.xml",
        [Parameter(Mandatory=$false)][string]$configurationFile = "DeploymentConfig.xml")
try
{
    #$deploymentTarget = "Dev"
    #$tasksFile = "DeploymentTasks.xml"
    #$configurationFile = "DeploymentConfig.xml"
    #$script:WorkingDirectory = "C:\Source\repos\it-audit-dashboard-angular-v12\master\Deployment"

    function Validate-params
    {
        if([System.IO.File]::Exists($script:tasksFileCompletePath) -eq $false) {
            Write-Log -outputContent "Tasks file could not be found: $script:tasksFileCompletePath"
            throw "Tasks file could not be found: $script:tasksFileCompletePath"               
        }    
        if(-not [System.String]::IsNullOrEmpty($script:configFileCompletePath) -and -not [System.IO.File]::Exists($script:configFileCompletePath)){
            Write-Log -outputContent "The configuration File could not be found: $script:configFileCompletePath"
            throw ("The configuration File could not be found: $script:configFileCompletePath")
        }
        if([System.IO.Directory]::Exists($script:scriptsFolderCompletePath ) -eq $false) {
            Write-Log -outputContent "Scripts Folder could not be found: $script:scriptsFolderCompletePath"
            throw ("Scripts Folder could not be found: $script:scriptsFolderCompletePath")
        }       
    }
    function Set-Globals {        
        $script:tasksFileCompletePath = [System.IO.Path]::Combine($script:WorkingDirectory, $tasksFile);
        $script:configFileCompletePath = [System.IO.Path]::Combine($script:WorkingDirectory, $configurationFile);
        $script:scriptsFolderCompletePath = [System.IO.Path]::Combine($script:WorkingDirectory, "TaskScripts");
        $script:ComputerName = $env:computername        
    }
    function Validate_ContentFromXMLFiles {
        if(-not $script:targetConfig) {
            throw ("The Environment Settings for $deploymentTarget could not be found...the name is case sensititve.")
        }       
    }
    function Should_WeRunOnThisServer {
        # If Taregt Servers are specified.
        if($script:targetServers.Count -gt 0) {
            if(($script:targetServers | Select-Object -ExpandProperty ServerName) -notcontains $script:ComputerName) {                
                Write-Host ("The Current server: $($script:ComputerName), is Not in the TargetServer list: $(($script:targetServers | Select-Object -ExpandProperty ServerName) -join (','))")
                Write-Host ("This installer is NOT being run...")
                return $false;                
            }
        }
        return $true
    }
    function Get-UserVariableInput {
        $script:targetConfig.Variables.Variable | % {
            $var = $_
            if($var.PromptIfEmpty -and $var.PromptIfEmpty -eq $true -and [string]::IsNullOrEmpty($var.Value)) {
                $var.Value = (Read-Host -Prompt "Please enter a value for variable: $($var.Name)").ToString()                
            }
        }
    }
    function Get-ContentFromXMLFiles {
        [xml]$script:tasksXML = Get-Content -Path $script:tasksFileCompletePath
        [xml]$script:configXML = Get-Content -Path $script:configFileCompletePath
        $script:targetConfig = $script:configXML.Settings.SelectSingleNode("//Settings/Environments/Environment[@DeploymentTargetEnum='$deploymentTarget']")
        $script:targetServers = $script:targetConfig.SelectNodes("./TargetServers/TargetServer")                       
        Write-Log -logFilePath $script:logPath -outputContent $script:targetConfig.Variables.Variable      
    }
    function Replace-ScriptSwitches ( [Parameter(Mandatory=$false)][string]$string = "" )
    {
        $string = $string.Replace("%MachineName%", $script:ComputerName)
        [regex]::Matches([string]$string,'%DateTimeNow[^%]*%') | % {
            $match = $_
            $dateString = $match.Value.trim("%").Replace("DateTimeNow","")
            $string = $string.Replace($match.Value, [DateTime]::Now.ToString($dateString))
        }
        [regex]::Matches([string]$string,'%StartDateTime[^%]*%') | % {
            $match = $_
            $dateString = $match.Value.trim("%").Replace("StartDateTime","")
            $string = $string.Replace($match.Value, $script:ScriptStartDateTime.ToString($dateString))
        }
        $string = $string.Replace("%WorkingDirectory%", $script:WorkingDirectory)
        $string = $string.Replace("%DeploymentTarget%", $deploymentTarget)
        $string = $string.Replace("%LogFilePath%", $script:logPath)
        return $string
    }
    function ReplaceFrom-Config ( [Parameter(Mandatory=$false)][string]$string = "" )
    {
        if($script:targetConfig -and $string) {
            $script:targetConfig.Variables.Variable | % {
                $name = $_.Name
                $value = $_.Value
                $string = $string.Replace("$name", $value)
            }
        }
        return $string
    }
    function Write-Log ([Parameter(Mandatory=$false)][string]$logFilePath, $outputContent, $ForegroundColor = "white") 
    {
        if($outputContent.GetType().Name -eq "String") {
            Write-Host -ForegroundColor $ForegroundColor $outputContent                        
            $outputContent | Format-Table | Out-File -FilePath $logFilePath -Append
        } else {
            $outputContent | Format-Table | Out-Host    
            $outputContent | Format-Table | Out-File -FilePath $logFilePath -Append
        }
    }
    function Run-PowershellTaskCheck ($powershellTask) {
        $RunTask_DeploymentTarget = $true
        if($powershellTask.Ignore -eq $true) { return $false }
        $HasDeploymentTargets = $powershellTask.SelectNodes("./DeploymentTargets/DeploymentTarget").Count -gt 0
        if($HasDeploymentTargets) {
            if($powershellTask.SelectNodes("./DeploymentTargets[@Include='Exclude']").Count -gt 0) {
                $RunTask_DeploymentTarget = -not ($powershellTask.SelectNodes("./DeploymentTargets[@Include='Exclude']/DeploymentTarget[@Name='$($deploymentTarget)']")).Count -gt 0
            } else {
                $RunTask_DeploymentTarget = ($powershellTask.SelectNodes("./DeploymentTargets[@Include='Include']/DeploymentTarget[@Name='$($deploymentTarget)']")).Count -gt 0
            }
        }     
        $taskTargetServers = $powershellTask.SelectNodes("./TargetServers/TargetServer")  
        if($taskTargetServers.Count -gt 0) {
            if(($taskTargetServers | Select-Object -ExpandProperty ServerName) -notcontains $script:ComputerName) {                
                Write-Host ("The Current server: $($script:ComputerName), is Not in the Task's TargetServer list: $(($taskTargetServers | Select-Object -ExpandProperty ServerName) -join (','))")
                $RunTask_DeploymentTarget = $false;                
            }
        }
        return $RunTask_DeploymentTarget
    }
    function Main-Function {
        param()
        $script:tasksXML.Installation.PowerShell | % {
            $powershellTask = $_                                     
            if((Run-PowershellTaskCheck -powershellTask $powershellTask) -eq $true) {
                Write-Log -ForegroundColor White -logFilePath $script:logPath -outputContent $powershellTask
                $argumentList = @()
                $powershellTask.Parameters.Parameter | % {
                    $name = $_.Name
                    $value = ReplaceFrom-Config -string $_.Value                    
                    $value = Replace-ScriptSwitches -string $value  
                    $value = ReplaceFrom-Config -string $value                  
                    $argumentList += ("-$name", "`"$value`"")
                }
                Write-Log -ForegroundColor White -logFilePath $script:logPath -outputContent $argumentList
                $scriptPath = [System.IO.Path]::Combine($script:scriptsFolderCompletePath, $powershellTask.ScriptName)
                try{
                    Invoke-Expression "& `"$scriptPath`" $argumentList"
                } catch {
                    if($powershellTask.Vital -eq $true) {                    
                        throw $_
                    }
                    else {
                        Write-Log -ForegroundColor White -logFilePath $script:logPath -outputContent "Script Failed but Vital = False, so Continuing."                        
                    }
                }
            } else {
                Write-Log -ForegroundColor White -logFilePath $script:logPath -outputContent "Skipping Task: $($powershellTask.Name)"
            }
        }
    }
    $script:WorkingDirectory = $PSScriptRoot
    if([string]::IsNullOrEmpty($script:WorkingDirectory))
    {
        throw ("PSScript Root could not be resolved. the Working Directory could not be set.")
    }
    $script:logPath = [System.IO.Path]::Combine($script:WorkingDirectory, "InstallLog_$((Get-Date).ToString("ddMMMyyy_HHmmss")).txt")
    $script:ScriptStartDateTime = [DateTime]::Now
    Write-Log -logFilePath $script:logPath -outputContent "Running Installer..."
    Write-Log -logFilePath $script:logPath -outputContent "    Target Env Config: $deploymentTarget"
    Write-Log -logFilePath $script:logPath -outputContent "    Task File Path:    $script:tasksFileCompletePath"
    Write-Log -logFilePath $script:logPath -outputContent "    Config File Path:  $script:configFileCompletePath"
    Write-Log -logFilePath $script:logPath -outputContent "    Log file:          $($script:logPath)"    
    Set-Globals    
    Validate-params
    Get-ContentFromXMLFiles
    Validate_ContentFromXMLFiles
    if(Should_WeRunOnThisServer -eq $true) {
        Get-UserVariableInput
        Main-Function
        Write-Log -logFilePath $script:logPath -outputContent "Installer Completed. Log file here: $($script:logPath)"
    }
} catch {
    Write-Log -logFilePath $script:logPath -outputContent $_.Exception.ToString()                           
    Read-Host -Prompt "The above error occurred. Press Enter to exit."
}